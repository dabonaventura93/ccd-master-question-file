#!/usr/bin/env python

import socket, struct
from cryptography.fernet import Fernet

# Refer to README.md for the problem instructions

dest = ('network-server', 1337)

cmd_list = {
    'key-request' : 0x800,
    'key-provide' : 0x801,
    'encrypted-message' : 0x802,
    'error' : 0x8FF
}

#
#  You may wish to write helper functions here
# 

#This function should return the server's final message as an actual Python 3 string,
#not as a byte sequence.
def get_message_using_encrypted_request_protocol():
    pass #You'll want to delete this line after you add your code


if __name__ == "__main__":
    message = get_message_using_encrypted_request_protocol()
    print(message)
