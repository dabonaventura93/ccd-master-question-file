#!/usr/bin/env python
# Refer to README.md for the problem instructions

import socket, struct

dest = ('network-server', 1337)
accept_port_1 = 12345
accept_port_2 = 54321

cmd_list = {
    'success' : 0x800,
    'error' : 0x801,
    'query_mode' : 0x802,
    'get_key' : 0x803,
}

padding = 0x0000

def query_mode():
    pass

def get_key(recv):
    pass


