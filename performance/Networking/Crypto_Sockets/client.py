import socket
import re
import binascii
from cryptography.fernet import Fernet

# Refer to README.md for the problem instructions

HOST = 'network-server'  # The server's hostname or IP address
PORT = 5000        # The port used by the server


# Your create_fernet_and_find_sign_key function here

def dowork():
    # Your code here
    return


def main():
    dowork()


if __name__ == '__main__':
    main()
