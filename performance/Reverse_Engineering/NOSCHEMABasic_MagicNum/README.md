# KSAT List
This question is intended to evaluate the following topics:
- A0343: Conduct static analysis of a binary to determine its functionality.
- A0609: WinDbg Dynamic Analysis
- A0610: Disassemble Binary
- A0611: Static Analysis
- S0179: Reverse engineer a binary using IDA Pro.
- S0180: Reverse engineer a binary using WinDBG.

# Tasks
For this evaluation question, you must reverse engineer the BD_RE_EV_crackme.exe program. This program contains a 
function that will display a MessageBox once you have successfully solved this problem. You must figure out what 
conditions must be met in order for the MessageBox to appear.

The function containing the success box is looking for a certain value (the "magic number") in order to satisfy the 
success condition. Once you have discovered that value, modify the memory that contains that value that causes the 
control flow to reach the messagebox upon execution. 

It is not sufficient to just modify a jump instruction to make it reach the MessageBox. The MessageBox will display 
the magic number, and you will receive a Q- if an incorrect magic number is displayed in the MessageBox. When you are 
complete, leave the MessageBox with the correct magic number on your screen.
