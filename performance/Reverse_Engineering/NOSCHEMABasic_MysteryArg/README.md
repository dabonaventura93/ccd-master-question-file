# KSAT List
This question is intended to evaluate the following topics:
- A0343: Conduct static analysis of a binary to determine its functionality.
- A0610: Disassemble Binary
- A0611: Static Analysis
- A0609: WinDbg Dynamic Analysis
- S0179: Reverse engineer a binary using IDA Pro.
- S0180: Reverse engineer a binary using WinDBG.

# Tasks
For this evaluation question, you must reverse engineer the BD_RE_CR_crackme.exe program. This program contains a 
function that will display a MessageBox once you have successfully solved this problem. You must figure out what 
conditions must be met in order for the MessageBox to appear.

The function containing the MessageBox is looking for certain inputs in order to satisfy the success condition. Once 
you have discovered the correct input, run the program with that input and observe the MessageBox appear with Success! 
displayed.

Use IDA Pro and WinDBG to help you understand how the executable works. Experiment with different 
inputs and observe how the program responds. When you are complete, leave the MessageBox with the Success message 
displayed on the screen.
