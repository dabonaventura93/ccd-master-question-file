# Python: Exponential Euler
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0110: Implement error handling.

## Tasks
Implement the function `compute_euler_exp` that estimates the value of the mathematical constant 'e' (Euler's number).

**PARAMETERS:**
1. `exponent`: The power value to raise e to (x)

**RETURN:** The calculated approximate value of e^x rounded to **5 decimal places**; return `Invalid Input` if 
`exponent`is less than 0 or not an int

- Your function stops after summing 10 terms.

## Computing Euler's number
Use the formula:

e^x = 1 + x/1! + x^2/2! + x^3/3! + ... + x^n/n!

Note, n! means the factorial of n: n! = n * (n-1) * (n-2) * ... * (n - (n - 1)).

For example 5! = 5 * 4 * 3 * 2 * 1 = 120

**Note:**
THE SOLUTION FOR THIS PROBLEM IS BASED ON A SPECIFICALLY DEFINED ALGORITHM EXPLAINED BELOW. ALTHOUGH THE AUTOMATED 
TESTS MAY BE SUCCESSFUL, YOUR SOLUTION WILL RECEIVE A MANUAL REVIEW FOR FINAL DETERMINATION OF PASS/FAIL.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
