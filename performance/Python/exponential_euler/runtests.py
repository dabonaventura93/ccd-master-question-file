import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_compute_euler_exp_corner_cases(self):
        self.assertEqual("Invalid Input", compute_euler_exp("10"))
        self.assertEqual("Invalid Input", compute_euler_exp("-10"))
        self.assertEqual("Invalid Input", compute_euler_exp(-10))
        self.assertEqual(1, compute_euler_exp(0))

    def test_compute_euler_exp(self):
        self.assertEqual(2.71828, compute_euler_exp(1))
        self.assertEqual(7.38871, compute_euler_exp(2))
        self.assertEqual(20.06339, compute_euler_exp(3))
        self.assertEqual(143.68946, compute_euler_exp(5))
        self.assertEqual(10086.57319, compute_euler_exp(10))


if __name__ == '__main__':
    unittest.main()
