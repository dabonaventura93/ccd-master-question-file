# Python: Perfect Squares
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0026: Utilize standard library modules.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0047: Implement a function that returns multiple values.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the `findPerfectSquares` function to: find all perfect numbers (defined below) in a given list of numbers, 
compute the square root of the perfect squares, place the computed square roots in a new sorted ascending list, and 
determine the sum of the numbers in the new list.

**PARAMETERS:**
1. `numbers`: A list of numbers to search for perfect numbers

**RETURN:** two values: a list containing the sorted ascending square roots of the perfect squares and the sum of all 
of the numbers in that list

- Compute the square root of each number to check if it is a perfect square.
- Do not use a lookup table.
- If there are no perfect squares or the provided list is empty, return an empty list and 0.

### Perfect Squares
Perfect squares are numbers that are created when you multiply a whole number by itself. Ex: 4 is a perfect square 
because 2 times 2 is 4.

### Example
The `findPerfectSquares` function receives a list with the following sequence of numbers: 36, 3, 10, 9, 24, 16, and 36. 
After computation, `findPerfectSquares` returns a new list containing the sequence 3, 4, 6, and 6; and the integer 19.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
