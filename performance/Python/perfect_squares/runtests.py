import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_squares(self):
        numbers = [36,3,6,9,12,16,20,25,36,38]
        sqrs, tot = findPerfectSquares(numbers)
        self.assertEqual(24, tot)
        self.assertEqual([3, 4, 5, 6, 6], sqrs)

    def test_empty_squares(self):
        sqrs, tot = findPerfectSquares([])
        self.assertEqual(0, tot)
        self.assertEqual([], sqrs)

    def no_perfect_tests(self):
        numbers = [38,3,6,19,112,26,20,20,46,38]
        sqrs, tot = findPerfectSquares(numbers)
        self.assertEqual(0, tot)
        self.assertEqual([], sqrs)

    def test_big_list(self):
        numbers = [74,250,16,17,18,0,1,2,900,225,90,10000]
        sqrs, tot = findPerfectSquares(numbers)
        self.assertEqual(150, tot)
        self.assertEqual([0, 1, 4, 15, 30, 100], sqrs)


if __name__ == '__main__':
    unittest.main()
