import unittest
import shutil
from testfile import *

origin_dir = "original_files"
names_f = "names.txt"
brown_fox_f = "brownFox.txt"


class CheckRideTestOne(unittest.TestCase):
    def test_list(self):
        shutil.copyfile("/".join([origin_dir, names_f]), names_f)
        shutil.copyfile("/".join([origin_dir, brown_fox_f]), brown_fox_f)
        testList = ["Joe Jake Jamie John\n", "Sally Carrie Carla Bevis"]

        self.assertEqual("SUCCESS", manipulateFile(names_f, "Jake", "Jamie", "Bevis"))
        with open(names_f, "r") as nameFile:
            line = nameFile.readline()
            i = 0
            while line:
                self.assertEqual(line, testList[i])
                line = nameFile.readline()
                i += 1

        nameFile.close()

    def test_bad_names(self):
        self.assertEqual("FILE_ERROR", manipulateFile("namesBad.txt", "Jake", "Jamie", "Bevis"))

    def test_no_word(self):
        self.assertEqual("WORD_NOT_FOUND", manipulateFile(names_f, "Jerry", "Jamie", "Bevis"))
        
    def test_list_fox(self):
        testStr = "The quick sly brown fox jumped over the lazy dog's back fence"
        self.assertEqual("SUCCESS", manipulateFile(brown_fox_f, "quick", "sly", "fence"))
        with open(brown_fox_f, "r") as nameFile:
            line = nameFile.readline()
            self.assertEqual(line, testStr)


if __name__ == '__main__':
    unittest.main()
