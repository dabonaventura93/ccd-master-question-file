# Python: Manipulate File
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0037: Open and close an existing file.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0038: Read, parse, write (append, insert, modify) file data.
- S0033: Utilize assignment operators to update a variable.
- S0041: Determine location of content within a file.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0080: Demonstrate the skill to implement exception handling.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `manipulateFile` to search a file for a specified word so it can insert a word after the 
specified word and append another word at the end of the file. When the function is finished it returns a status code.

**PARAMETERS:**
1. `fileName`: the name of a file that you will read/write/append to/from
2. `findWord`: a word to search for in the file
3. `insertWord`: a word that will be inserted in the file
4. `appendWord`: a word that will be appended to the end of the file

**RETURN:** A string containing: "SUCCESS" on success, "WORD_NOT_FOUND" if `insertWord` was not in the file, or 
"FILE_ERROR" if there was an error reading or writing to the file

- If the string in `findWord` does not exist in the file, make no changes to the file.
- If the string in `findWord` exists in the file:
  - Insert a space and the string in `insertWord` immediately after the `findWord`.
  - Append a space and then the string in `appendWord` to the end of the file.

### EXAMPLE

For example, the file may contain the following text:

```text
The quick brown fox jumped 
```

If `findWord` contained `quick` and the `insertWord` contained `sly`, once processed the line in the file would have:

```text
The quick sly brown fox jumped
```

For example, the end of the file may contain:

```text
over the lazy dog's back
```

If `appendWord` contains `fence`, once processed the line in the file would have:

```text
over the lazy dog's back fence
```

## **NOTE:** During your development, should the data files under test get corrupted, pristine copies are located in the 
`original files` folder. Simply make a copy and bring over into your working directory.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).