# Python: Pythagorean Triples
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `find_pythagorean_triples` to find all of the Pythagorean triples where each side length is no 
larger than 20.

**PARAMETERS:**
- This function takes no parameters

**RETURN:** A list of tuples with each tuple containing the hypotenuse, side 1, and side 2 respectively

- Include both configurations for the sides in your list since both are valid (ie. 5, 4, 3 and 5, 3, 4).

### Pythagorean Triples.
A right triangle can have sides that are all integers. The set of three integer values for the sides of a right 
triangle is called a Pythagorean triple. These three sides must satisfy the relationship that the sum of the squares of 
two of the sides is equal to the square of the hypotenuse.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
