import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_find_pythagorean_triples(self):
        test_list = find_pythagorean_triples()
        self.assertTrue((5, 3, 4) in test_list)
        self.assertTrue((5, 4, 3) in test_list)
        self.assertTrue((10, 6, 8) in test_list)
        self.assertTrue((10, 8, 6) in test_list)
        self.assertTrue((13, 5, 12) in test_list)
        self.assertTrue((13, 12, 5) in test_list)
        self.assertTrue((15, 9, 12) in test_list)
        self.assertTrue((15, 12, 9) in test_list)
        self.assertTrue((17, 8, 15) in test_list)
        self.assertTrue((17, 15, 8) in test_list)
        self.assertTrue((20, 12, 16) in test_list)
        self.assertTrue((20, 16, 12) in test_list)


if __name__ == '__main__':
    unittest.main()
