import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_created_correctly(self):
        jokem = Player("Jokem")
        self.assertEqual("Jokem", jokem.name)
        self.assertEqual((0, 0), jokem.report_pos())
        self.assertEqual(100.0, jokem.hit_points)


class CheckRideTestTwo(unittest.TestCase):
    def test_move(self):
        jokem = Player("Jokem")
        coord = (5,5)
        jokem.move(5,5)
        self.assertEqual(coord, jokem.report_pos())
        jokem.move(25,50)
        self.assertEqual((30,55), jokem.report_pos())


class CheckRideTestThree(unittest.TestCase):
    def test_health(self):
        jokem = Player("Jokem")
        jokem.move(100,100)
        self.assertEqual(29.289321881345245, jokem.hit_points)

        self.assertEqual(jokem.move(100,100), ('You are out of hit points!'))


if __name__ == '__main__':
    unittest.main()
