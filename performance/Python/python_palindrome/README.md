# Python: Palindrome
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `palindrome` that determines if a ***five-digit*** integer is a palindrome.

**PARAMETERS:**
1. `number`: A 5 digit integer 

**RETURN:** The string `palindrome` if the number provided is a palindrome, and `NOT a palindrome` if the number does
not meet the criteria to be a palindrome (see below)

- Assume that all `number` values have 5 digits.

## Palindrome
A palindrome is a number or a text phrase that reads the same backwards or forwards. 
- e.g. The following five digit integers are a palindromes: 12321, 55555, 45554, and 11611. 

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
