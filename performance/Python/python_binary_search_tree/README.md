# Python: Binary Search Tree
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0074: Find an item in a Binary Search Tree.
- S0075: Add and remove nodes from a Binary Search Tree.
- S0052: Implement a function that returns a single value.
- S0060: Create and destroy a Binary Search Tree.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement functions that build a Binary Search Tree (BST) from a list of values and search a Binary Search Tree for 
a value in the list.
        
### Task 1
Implement the function `buildBST` that builds a Binary search tree out of a list of integers.

**PARAMETERS:**
1. `nums`: A list of integers to use as data in the BST nodes

**RETURN:** The root node of the BST

- The input list of numbers is not sorted.
- The numbers must be inserted into the BST in the same order as the list.
- Use the provided `Node` class and create a `Node` for each value.
- Each node needs to be placed in the correct position in the tree according to its value.
- If a value in the list is already in the BST, ignore the value and continue processing the rest of the list.
- Do not "balance" or re-order the BST. (This is a common performance improvement for BSTs but we specifically don't want that for this question)

### Task 2
Implement the function `findLevel` that will search a BST and find the level of the BST where the value resides. 

**PARAMETERS:**
1. `root`: The root of a binary search tree
2. `findVal`: An integer value to search within the binary search tree

**RETURN**: The level where the value resides within the tree or `None` if the value is not found

## Binary Search Tree

- The left child has a value less than its parent node's value. (In fact, all nodes in the left subtree will have 
  values less than the parent node's.)
- The right sub-tree of a node has a value greater than its parent node's value.
- In this scenario, no duplicate values are allowed.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
