import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def setUp(self):
        self.root = buildBST([7, 3, 8, 9, 2, 6])
        self.root2 = buildBST([1, 2, 3, 4, 7, 5, 4, 6])   

    def test_bst(self):
        self.assertEqual(self.root.val, 7)
        self.assertEqual(self.root.right.val, 8)
        self.assertEqual(self.root.right.right.val, 9)
        self.assertEqual(self.root.left.val, 3)
        self.assertEqual(self.root.left.right.val, 6)
        self.assertEqual(self.root.left.left.val, 2)

    def test_bst_2(self):
        self.assertEqual(self.root2.val, 1) 
        self.assertEqual(self.root2.right.val, 2) 
        self.assertEqual(self.root2.right.right.val, 3)
        self.assertEqual(self.root2.right.right.right.val, 4)
        self.assertEqual(self.root2.right.right.right.right.val, 7)
        self.assertEqual(self.root2.right.right.right.right.left.val, 5)
        self.assertEqual(self.root2.right.right.right.right.left.right.val, 6)

    def test_findLevel(self):
        self.assertEqual(findLevel(self.root,6),2)
        self.assertEqual(findLevel(self.root,16),None)

        self.assertEqual(findLevel(self.root2,6), 6)
        self.assertEqual(findLevel(self.root2,4), 3)
        self.assertEqual(findLevel(self.root2,8), None)


if __name__ == '__main__':
    unittest.main()
