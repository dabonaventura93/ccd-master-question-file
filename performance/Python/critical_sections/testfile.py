import threading, time, random

# Refer to README.md for the problem instructions


class AddThread( threading.Thread ):
    pass


class MultiplyThread( threading.Thread ):
    pass


if __name__ == "__main__":
    number = 1

    lock = threading.RLock()

    thread1 = AddThread("thread1", lock)
    thread2 = MultiplyThread("thread2", lock)

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()

    print("Number is: {}".format(number))
