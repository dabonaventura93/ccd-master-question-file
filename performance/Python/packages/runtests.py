import unittest
import pkg_resources
from testfile import *
import os


class TestNavigateWorld(unittest.TestCase):
    def test_astar_installed(self):
        installed_packages = pkg_resources.working_set
        installed_packages_list = [f"{i.key}" for i in installed_packages]
        self.assertTrue("astar-python" in installed_packages_list)

    def test_in_venv(self):
        self.assertTrue(os.getenv("VIRTUAL_ENV") is not None)

    def test_navigate_world(self):
        smol_world = [
            [0, 1, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]
        ded_world = []
        borken_world = [
            [0, 0, 1, 2, 0],
            [0, 1, 0, 0, 0, 0, 8],
            [1, 0, 2, 3, 4],
            [1, 0, 0, 0, 2, 0],
            [0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0]
        ]
        polluted_world = [
            [0, 3, "A", 0, 0],
            [1, 0, 2, 3, 4],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0]
        ]
        time_warped_world = [
            [1, 0, 2, 3, 4],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 3, -4, 0, 0],
            [0, 1, 0, 0, 0]
        ]
        perfect_world = [
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0]
        ]
        good_world = [
            [0, 3, 0, 0, 0],
            [1, 7, 2, 3, 4],
            [1, 3, 0, 0, 0],
            [0, 0, 0, 6, 0],
            [0, 1, 9, 0, 0]
        ]
        holey_world = [
            [0, 0, None, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [None, None, None, None, None, 0],
            [0, 0, 0, 0, None, 0],
            [0, 0, 0, 0, 0, 0],
            [0, None, 0, None, None, None],
            [0, 0, 0, 0, 0, 0]
        ]
        canyon_world = [
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0],
            [0, 0, 0, None, 0, 0]
        ]

        start = [0, 0]
        self.assertEqual(navigate_world(smol_world, start, [2, 2]), None)
        self.assertEqual(navigate_world(ded_world, start, [2, 2]), None)
        self.assertEqual(navigate_world(borken_world, start, [4, 5]), None)
        self.assertEqual(navigate_world(polluted_world, start, [4, 4]), None)
        self.assertEqual(navigate_world(time_warped_world, start, [4, 4]), None)
        self.assertEqual(navigate_world(perfect_world, start, [5, 6]), [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], 
                         [5, 5], [5, 6]])
        self.assertEqual(navigate_world(good_world, start, [4, 4]), [[0, 0], [0, 1], [0, 2], [1, 3], [2, 3], [3, 4], 
                         [4, 4]])
        self.assertEqual(navigate_world(holey_world, start, [5, 6]), [[0, 0], [1, 1], [2, 1], [3, 1], [4, 1], [5, 2], 
                         [5, 3], [4, 4], [3, 4], [2, 5], [3, 6], [4, 6], [5, 6]])
        self.assertEqual(navigate_world(canyon_world, start, [5, 6]), None)


if __name__ == '__main__':
    unittest.main()
