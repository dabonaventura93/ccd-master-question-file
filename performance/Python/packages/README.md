# Python: Packages
## KSAT List
This question is intended to evaluate the following topics:
- S0027: Install for a Python package using PIP.
- S0028: Set up and replicate a Python Virtual Environment.
- S0024: Declare and/or implement container data type.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.

## Tasks
For this problem you will need to create a virtual environment, install a package, and then use the package to find a 
path in a grid.

### Task 1
Create a virtual environment inside this question's directory.

### Task 2
Use pip to install the python package `astar_python` inside of the virtual environment created in Task 1.

- Your virtual environment setup steps need to be in the `env_setup.sh` bash script.
- Do not remove the line ```python3 runtests.py```.

### Task 3
Implement the function `navigate_world` that will use the `A*` algorithm in `astar_python` to traverse a multi 
dimensional array. 

**PARAMETERS:**
1. `world`: A 2 dimensional list containing integers representing the cost to move into that element
2. `start`: A list containing the [col, row] to start
3. `finish`: a list containing the [col, row] to traverse to

**RETURN:** the list of points from start to finish, or `None` if there was no path to finish or if `world` is invalid

- Use the functions provided in the `astar_python` module to find the path.
- The `world` sizes must be at least 5x5.
- `world` must not be a jagged array.
- `world` can only contain 0, positive numbers, or None.
- The tests must pass inside the virtual environment created in Task 1.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
