# Python: Binary to Decimal
## KSAT List
This question is intended to evaluate the following topics:
- A0061: Create and implement functions to meet a requirement.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0080: Demonstrate the skill to implement exception handling.
- S0079: Validate expected input.
- S0113: Sanitize input data.
- S0110: Implement error handling.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement a function `binary_to_decimal` that takes an integer containing 0s and 1s (i.e., a `binary` integer) and 
returns its decimal equivalent.

**PARAMETERS:**
1. `binaryNumber`: an int representing a binary number

**RETURN:** an int converted to the binary number's base 10 equivalent, or `"Invalid Input"` if `binaryNumber` is not 
binary

### Example:
The function `binary_to_decimal` receives an int with the value `1101` and returns an int with the value `13`.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
