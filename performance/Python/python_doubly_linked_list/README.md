# Python: Doubly Linked List
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0067: Find an item in a Doubly Linked List.
- S0068: Add and remove nodes from a Doubly Linked List.
- S0052: Implement a function that returns a single value.
- S0056: Create and destroy a Doubly Linked List.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement a doubly linked list where nodes can be added or removed.

### Task 1
Implement the function `buildList` that iterates through a list of values and uses them to create a doubly-linked list
with nodes that link to their next and previous Nodes.

**PARAMETERS:**
1. `names`: A list of strings of people's first names

**RETURN:** The head node of the doubly-linked list

- Use the Node class defined in `testfile.py` to create the doubly-linked list.
- Create a node for each name in the list.
- Each name needs to be added to the head of the doubly-linked list.
- Add names to the list in the same order given in `names`

### Task 2
Implement the function `removeName` that will search a doubly-linked list for a name and remove that node from the list.

**PARAMETERS:**
1. `head`: The head node of a doubly-linked list created in Task 1
2. `findName`: A string of a person's name that may be in the linked-list

**RETURN:** The head node after removing the name

- If the name is not found, make no changes to the list.
- If the head node is the node removed, return the node that is the new head.

### Task 3
Implement the function `appendName` that creates a new node and appends that node to the end of the doubly-linked list.

**PARAMETERS:**
1. `head`: The head node of the doubly-linked list created in Task 1
2. `addName`: A string of a person's name that will be added to the linked list

**RETURN:** The head node after appending the node with the new name

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
