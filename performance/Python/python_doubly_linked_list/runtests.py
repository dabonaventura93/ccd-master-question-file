import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_build_list(self):
        # TASK 1

        names = ['Bill', 'Barney', 'Joe', 'Sally', 'Sarah', 'Cathy', 'Henry', 'Rick']
        head = buildList(names)
        curr = head
        prev = head
        names = ['Rick', 'Henry', 'Cathy', 'Sarah', 'Sally', 'Joe', 'Barney', 'Bill']
        # check list from beginning to end
        for name in names:
            self.assertEqual(name, curr.name)
            prev = curr
            curr = curr.next

        # ensure last node's next points to None
        self.assertEqual(curr, None)

        names = ['Bill', 'Barney', 'Joe', 'Sally', 'Sarah', 'Cathy', 'Henry', 'Rick']
        # check list from end to beginning
        for name in names:
            self.assertEqual(name, prev.name)
            prev = prev.prev    

        # ensure head node's prev points to None
        self.assertEqual(prev, None)

    def test_remove_node(self):
        # TASK 2
        names = ['Bill', 'Barney', 'Joe', 'Sally', 'Sarah', 'Cathy', 'Henry', 'Rick']
        head = buildList(names)
        findName = 'Sally'
        names_valid = ['Rick', 'Henry', 'Cathy', 'Sarah', 'Joe', 'Barney', 'Bill']

        head = removeName(head, findName)
        curr = head
        prev = head
        # check list from beginning to end
        for name in names_valid:
            self.assertEqual(name, curr.name)
            prev = curr
            curr = curr.next

        # ensure last node's next points to None
        self.assertEqual(curr, None)

        names_valid = ['Bill', 'Barney', 'Joe', 'Sarah', 'Cathy', 'Henry', 'Rick']
        # check list from end to beginning
        for name in names_valid:
            self.assertEqual(name, prev.name)
            prev = prev.prev

        # ensure head node's prev points to None
        self.assertEqual(prev, None)

    def test_add_node(self):
        # TASK 3
        names = ['Bill', 'Barney', 'Joe', 'Sarah', 'Cathy', 'Henry', 'Rick']
        head = buildList(names)
        addName = 'Allen'
        names_valid = ['Rick', 'Henry', 'Cathy', 'Sarah', 'Joe', 'Barney', 'Bill', 'Allen']

        head = appendName(head, addName)
        curr = head
        prev = head

        # check list from beginning to end
        for name in names_valid:
            self.assertEqual(name, curr.name)
            prev = curr
            curr = curr.next

        # ensure last node's next points to None
        self.assertEqual(curr, None)

        names_valid = ['Allen', 'Bill', 'Barney', 'Joe', 'Sarah', 'Cathy', 'Henry', 'Rick']
        # check list from end to beginning
        for name in names_valid:
            self.assertEqual(name, prev.name)
            prev = prev.prev

        # ensure head node's prev points to None
        self.assertEqual(prev, None)

    def test_single_node_remove(self):
        head = buildList(['Allen'])
        head = removeName(head, 'Allen')
        self.assertEqual(head, None)


if __name__ == '__main__':
    unittest.main()
