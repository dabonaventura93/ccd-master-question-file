import unittest
import os, time, subprocess


class CheckRideTestOne(unittest.TestCase):
    def test_stack(self):
        subprocess.run("python3 testfile.py 1 3 4.7 hello 5.4 2 hi".split())

        try:
            f = open('output.txt','r')
            num = f.read()
            self.assertEqual(num.strip(), '23')
            f.close()
        except IOError:
            #unable to open file
            self.assertEqual(1, 2)

        subprocess.run("python3 testfile.py I want to count this".split())

        try:
            f = open('output.txt','r')
            num = f.read()
            self.assertEqual(num.strip(), '16')
            f.close()
        except IOError:
            #unable to open file
            self.assertEqual(1, 2)

        subprocess.run("python3 testfile.py 45 45.51 7 cat hello 57.3 985".split())

        try:
            f = open('output.txt','r')
            num = f.read()
            self.assertEqual(num.strip(), '1148')
            f.close()
        except IOError:
            #unable to open file
            self.assertEqual(1, 2)

        subprocess.run("python3 testfile.py ? will these 4 be ok 77.2".split())

        try:
            f = open('output.txt','r')
            num = f.read()
            self.assertEqual(num.strip(), '95')
            f.close()
        except IOError:
            #unable to open file
            self.assertEqual(1, 2)


        subprocess.run("python3 testfile.py four".split())

        try:
            f = open('output.txt','r')
            num = f.read()
            self.assertEqual(num.strip(), '4')
            f.close()
        except IOError:
            #unable to open file
            self.assertEqual(1, 2)


if __name__ == '__main__':
    unittest.main()
