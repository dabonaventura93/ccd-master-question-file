import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_stack(self):
        myStack = stack()

        # popping from an empty stack
        self.assertEqual("STACK TOO SMALL", myStack.popStack(2))

        myStack.pushStack([5, 2, 7, 9])

        self.assertEqual(myStack.size, 4)

        # only 4 items on stack
        self.assertEqual("STACK TOO SMALL", myStack.popStack(5))

        top = myStack.top

        items = [9, 7, 2, 5]
        i = 0
        # step through stack from top to bottom and compare with items
        while top is not None:
            self.assertEqual(top.num, items[i])
            top = top.next
            i += 1

        # make sure there were 4 items on stack
        self.assertEqual(i, 4)
        items = [9, 7]

        self.assertEqual(items, myStack.popStack(2))

        self.assertEqual(myStack.size, 2)

        top = myStack.top

        items = [2, 5]
        i = 0
        # step through stack from top to bottom and compare with items
        while top is not None:
            self.assertEqual(top.num, items[i])
            top = top.next
            i += 1
        # make sure there were 2 items on stack
        self.assertEqual(i, 2)
        myStack.pushStack([0, 22, 32, 12, 99])

        self.assertEqual(myStack.size, 7)
        top = myStack.top

        items = [99, 12, 32, 22, 0, 2, 5]
        i = 0
        # step through stack from top to bottom and compare with items
        while top is not None:
            self.assertEqual(top.num, items[i])
            top = top.next
            i += 1

        self.assertEqual(i, 7)

        myStack.emptyStack()

        self.assertEqual(myStack.top, None)

        myStack.pushStack([7, 6, 5, 4, 3, 2, 1])

        top = myStack.top

        items = [1, 2, 3, 4, 5, 6, 7]
        i = 0
        # step through stack from top to bottom and compare with items
        while top is not None:
            self.assertEqual(top.num, items[i])
            top = top.next
            i += 1
        # make sure there were 7 items on stack
        self.assertEqual(i, 7)


if __name__ == '__main__':
    unittest.main()
