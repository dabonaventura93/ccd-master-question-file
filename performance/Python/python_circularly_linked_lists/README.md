# Python: Circularly linked List
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0069: Find an item in a Circularly Linked List.
- S0070: Add and remove nodes from a Circularly Linked List.
- S0052: Implement a function that returns a single value.
- S0057: Create and destroy a Circularly Linked List.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0110: Implement error handling.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `buildList` that uses a list of integers to create a circularly linked list of `Node` from 
`testfile.py`.

**PARAMETERS:**
1. `nums`: A list of numbers to build the circularly linked list

**RETURN:** The root node of the circularly linked list or `None` if `nums` is an empty list

- Iterate the `nums` list and insert the numbers into nodes.
- Each new node will be inserted at the head of the circularly linked list.
- If a number in `nums` is already in the circularly linked list, do not insert it into the circularly linked list.
- If an item in `nums` is not an integer, do not insert it into the circularly linked list.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
