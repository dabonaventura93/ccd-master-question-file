import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_list1(self):
        l = [90,80,70,60,50]
        l2 = [50,60,70,80,90]

        curr = buildList(l)

        for num in l2 :
            self.assertEqual(curr.num, num)
            curr = curr.next
        self.assertEqual(curr.num,50) #last node points to first

    def test_list2(self):
        l = [3,5,6,7,3,5]
        l2 = [7,6,5,3]

        curr = buildList(l)

        for num in l2 :
            self.assertEqual(curr.num, num)
            curr = curr.next
        self.assertEqual(curr.num,7) #last node points to first

    def test_list3(self):
        l = [8, "blue", 44.5, 9, 2, 2, 0]
        l2 = [0,2,9,8]

        curr = buildList(l)

        for num in l2 :
            self.assertEqual(curr.num, num)
            curr = curr.next
        self.assertEqual(curr.num, 0) #last node points to first

    def test_list4(self):
        curr = buildList([])
        self.assertEqual(curr, None)    

    def test_list5(self):
        curr = buildList([10])  # one node circular linked list
        self.assertEqual(curr.num, 10)
        self.assertEqual(curr.next.num, 10)    


if __name__ == '__main__':
    unittest.main()
