import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_get_number_from_string(self):
        self.assertEqual(19.15, get_number_from_string("There is number 19.15 in this string"))
        self.assertEqual(-43.85, get_number_from_string("There is a negative number -43.85 here"))
        self.assertEqual(-34.65, get_number_from_string(" -34.65 "))
        self.assertEqual(-34.0, get_number_from_string(" -34.0 "))
        self.assertEqual(-34.0, get_number_from_string(" .5 -34.0 "))
        self.assertEqual(2.5, get_number_from_string(" 2.5 -64.0 "))

    def test_no_number(self):
        self.assertEqual("No number", get_number_from_string("six"))
        self.assertEqual("No number", get_number_from_string("-34.65"))
        self.assertEqual("No number", get_number_from_string(" -34.65"))
        self.assertEqual("No number", get_number_from_string("-34.65 "))
        self.assertEqual("No number", get_number_from_string(" --34.65 "))
        self.assertEqual("No number", get_number_from_string(" -34. "))
        self.assertEqual("No number", get_number_from_string(" -34..0 "))
        self.assertEqual("No number", get_number_from_string(" .6 "))


if __name__ == '__main__':
    unittest.main()
