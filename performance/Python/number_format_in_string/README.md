# Python: Number Format in String
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0026: Utilize standard library modules.
- S0023: Declare and implement data types.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `get_number_from_string` that searches a string for a number.

**PARAMETERS:**
1. `testString`: A string that contains a number within the string

**RETURN:** The first number in the string as a floating point value or the string `No number` if a number was not 
found

- A number can have any number of digits, but it can have only digits and a decimal point. 
- The decimal point is optional but, if it appears in the number, there must be only one.
- The decimal point must have digits on its left and its right.
- There must be whitespace on both sides of a valid number.
- Negative numbers are preceded by a minus sign. 

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
