# Python: File Tasks
## KSAT List
This question is intended to evaluate the following topics:
- A0047: Implement file management operations.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0037: Open and close an existing file.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0038: Read, parse, write (append, insert, modify) file data.
- S0040: Determine the size of a file.
- S0039: Create and delete a file.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0080: Demonstrate the skill to implement exception handling.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0110: Implement error handling.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `writeNewFile` that reads a file, makes small edits on each line if needed, then writes the new contents to a new file.

**PARAMETERS:**
1. `fileName`: the name of a text file to read

**RETURN:** the original file's size in bytes, `-1` if the file does not exist, or `-2` if the file is empty

- The name of the new file to write to should be the same name as the file being read, prepended with the text `new`.
  - For example, if the name in `filename` is `story.txt`, the file that you write to should be named `newstory.txt`.
- Each line of the file in `fileName` will contain a single sentence but some of the sentences may contain one of the 
  following errors:
  - Some may not begin with a capital letter.
  - Some may be missing a period at the end.
- When writing to the new file, ensure all sentences begin with a capital letter and end with a period.
- Ensure each sentence ends with a newline character `\n` except for the last sentence in the file.
- Once the new file is written, delete the original file named `fileName`.
- Do not delete or create files for file not found and empty file error cases.

**NOTE:**
A copy of the original files to read are located in the original_files directory. You may make copies of these files 
and place them in the working directory as needed to rerun your tests.

### Example

```text
This is sentence one.\n
This is sentence two.\n
This is the last sentence.(EOF)
```

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
