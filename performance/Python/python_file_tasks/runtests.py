import unittest
import os
import shutil
from testfile import *

TEST1_CHECK = "This is one sentence.\nThis is another sentence.\nThis is the last sentence."
TEST2_CHECK = "Blue whale are huge.\nThey are the largest mammal to ever live.\nThey make a huge splash.\nThey can communicate with each other from miles away."
TEST3_CHECK = "All the sentences in this file are good.\nThey all begin with capital letters.\nThey all end with a period.\nThe output file should be identical.\nGood luck with this problem."


def get_file_conts(file_name):
    with open(file_name) as test_file:
        file_conts = test_file.read()
    return file_conts


class CheckRideTestOne(unittest.TestCase):
    def test_file1(self):
        # Test for test1.txt
        res = writeNewFile('test1.txt')
        self.assertEqual(res, 72)
        try:
            test1_data = get_file_conts("newtest1.txt")
            self.assertEqual(test1_data, TEST1_CHECK)
        except FileNotFoundError:
            self.fail("newtest1.txt could not be read. Verify that it exists.")
        
        self.assertEqual(os.path.exists("test1.txt"),False) #file should be gone

    def test_file2(self):
        # Test for test2.txt
        res = writeNewFile('test2.txt')
        self.assertEqual(res, 137)
        try:
            test2_data = get_file_conts("newtest2.txt")
            self.assertEqual(test2_data, TEST2_CHECK)
        except FileNotFoundError:
            self.fail("newtest2.txt could not be read. Verify it exists.")

        self.assertEqual(os.path.exists("test2.txt"),False) #file should be gone

    def test_file3(self):
        # Test for test3.txt
        res = writeNewFile('test3.txt')
        self.assertEqual(res, 171)
        try:
            test3_data = get_file_conts("newtest3.txt")
            self.assertEqual(test3_data, TEST3_CHECK)
        except FileNotFoundError:
            self.fail("newtest3.txt could not be read. Verify it exists.")

        self.assertEqual(os.path.exists("test3.txt"),False) #file should be gone

    def test_non_exist(self):
        # Test for non existent file
        res = writeNewFile('youcantfindme.txt')
        self.assertEqual(res, -1)

        self.assertEqual(os.path.exists("newyoucantfindme.txt"),False) # file shouldn't exist

    def test_empty(self):
        # Test for empty.txt
        res = writeNewFile('empty.txt')
        self.assertEqual(res, -2)
        self.assertEqual(os.path.exists("newempty.txt"),False) # file shouldn't exist    


if __name__ == '__main__':
    unittest.main()
