# KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0037: Open and close an existing file.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0038: Read, parse, write (append, insert, modify) file data.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0080: Demonstrate the skill to implement exception handling.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0110: Implement error handling.
- S0082: Implement conditional control flow constructs.

# Tasks
Write the function `find_culprits` that receives the name of a file. The function will open and read the file. The 
file contains a list of user ids and number of minutes their login session has been idle. The file can contain 
multiple session entries of the same user id. The format of the file is: userid, mins.

Example:

```text
jschmo, 22
haaron, 12
haaron, 7
jschmo, 17
```

The function should find (up to) the top five users with the most total idle time and save them in order (highest to 
lowest) in a list of lists. Each item in the list will contain a userid and number of mins as an integer in that order.

Example: [["jschmo",39],["haaron",19]]

Write no more than the top five users to the list handling the case where there may not be five total users.
- If there is a tie between values any order between the ties is valid.
- If the file cannot be opened, return None.
- If the file processes successfully, return the list of lists of users.

For this exercise all data in the file has valid format/content.
