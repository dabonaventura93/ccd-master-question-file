# Python: IP Port Match
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0026: Utilize standard library modules.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.
- S0161: Write and utilize regular expressions in a program.

## Tasks
Implement the function `find_matches` that uses a regular expression to extract IP/port pairs from a string containing 
a connection log.

**PARAMETERS:**
1. `inputString`: A string containing a log with ip port combinations

**RETURN:** A list of tuples with the format (address, port), where `address` is a string and `port` is an integer

- Valid IP/port combinations are in the format `<ip_address>:<port>`
- The `ip_address` is an IPv4 style address
- The `port` is an integer in the range 50000-62000 (inclusive)
- Use a regular expression to find the IP address and port combinations

**NOTE:** 
THE SOLUTION FOR THIS PROBLEM IS BASED ON A SPECIFICALLY DEFINED METHOD EXPLAINED ABOVE. ALTHOUGH THE AUTOMATED 
TESTS MAY BE SUCCESSFUL, YOUR SOLUTION WILL RECEIVE A MANUAL REVIEW FOR FINAL DETERMINATION OF PASS/FAIL.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).