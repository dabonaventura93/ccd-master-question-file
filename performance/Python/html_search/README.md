# Python: HTML Search
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0026: Utilize standard library modules.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `locate_URLs` that searches through an HTML string to locate ALL valid URLs.

**PARAMETERS:**
1. `hString`: a string that may contain URLs

**RETURN:** A list of strings containing the stripped URLs, or the string 'No valid URLs' if the input string has no 
URLs

- Assume that a valid URL is enclosed in quotes and begins with "http://".

### Example
If the input string is `<a href = "http://www.ipsecureinc.com">`, the function will return a list containing 
`http://www.ipsecureinc.com`. 

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).