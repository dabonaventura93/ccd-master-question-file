s1 =\
'''
<html>
<body>

<h1>My First Heading</h1>

<p>My first paragraph.</p>

</body>
</html>
'''

s2 =\
'''
<html>
<body>

<a href="http://www.ipsecureinc.com">This is a link</a>

</body>
</html>
'''

s3 =\
'''
<html>
<body>

<a href="http://www.w3schools.com">w3schools</a>
<a href="http://www.stmarytx.edu">St. Mary's University</a>
<a href="http://www.irs.gov"> IRS </a>
<a href="http://www.cloudfront.net"> Cloud Front </a>
<a href="http://www.wikipedia.org"> Wikipedia </a>

</body>
</html>
'''

HTML_files=[]
HTML_files.append(s1)
HTML_files.append(s2)
HTML_files.append(s3)
