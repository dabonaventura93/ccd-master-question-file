import unittest
from testfile import *
from data import *


class CheckRideTestOne(unittest.TestCase):
    def test_locate_URLs(self):
        self.assertEqual("No valid URLs", locate_URLs(HTML_files[0]))
        self.assertEqual(['"http://www.ipsecureinc.com"'], locate_URLs(HTML_files[1]))
        self.assertEqual(['"http://www.w3schools.com"',
                          '"http://www.stmarytx.edu"',
                          '"http://www.irs.gov"',
                          '"http://www.cloudfront.net"',
                          '"http://www.wikipedia.org"'], locate_URLs(HTML_files[2]))


if __name__ == '__main__':
    unittest.main()
