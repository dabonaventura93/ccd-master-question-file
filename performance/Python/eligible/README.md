# Python: Eligible
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0080: Demonstrate the skill to implement exception handling.
- S0079: Validate expected input.
- S0110: Implement error handling.
- S0082: Implement conditional control flow constructs.

## Tasks 
Implement the `getEligibility` function that determines the eligibility a person has.

**PARAMETERS:**
1. `year`: The person's year of birth
2. `month`: The month of the person's birth
3. `day`: The day of a person's birth

**RETURN:** A set object containing the eligibilities a person has depending on age

- For this problem, assume today is July 4, 2019.
- Include every item the person is eligible for in the set.
- The eligibility thresholds are:
  - \< 16  "Ineligible"
  - \>= 16 "Drive"
  - \>= 18 "Vote"
  - \>= 21 "Drink"
  - \>= 55 "Discount"
  - \>= 67 "Retire"
- If a person is less than 16 the set will contain: `{"Ineligible"}`
- If an invalid year, month, or day is received as a parameter return an empty set.
- If the passed date is greater than July 4, 2019 return an empty set.

### Example:
For a person born on June 1, 1986, they would be 33, and the set will contain: `{"Drive","Vote","Drink"}`.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).