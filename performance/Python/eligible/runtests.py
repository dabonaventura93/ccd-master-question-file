import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_eligible(self):
        res = getEligibility(1965, 7,2)
        e1 = {"Drive","Drink","Vote"}
        self.assertEqual(e1, res)

        res = getEligibility(1964, 7,2)
        e1 = {"Drive","Drink","Vote","Discount"}
        self.assertEqual(e1, res)

        res = getEligibility(1964, 7,5)
        e1 = {"Drive","Drink","Vote"}
        self.assertEqual(e1, res)

        res = getEligibility(2003, 7,2)
        e1 = {"Drive"}
        self.assertEqual(e1, res)

        res = getEligibility(2003, 7,6)
        e1 = {"Ineligible"}
        self.assertEqual(e1, res)
        
    def test_bad_date(self):
        res = getEligibility("x", 7,2)
        e1 = set()
        self.assertEqual(e1, res)
        
        res = getEligibility(2019, 7, 0)
        self.assertEqual(e1, res)

    def test_too_recent(self):
        e1 = set()
        res = getEligibility(2019, 7,5)
        self.assertEqual(e1, res)


if __name__ == '__main__':
    unittest.main()
