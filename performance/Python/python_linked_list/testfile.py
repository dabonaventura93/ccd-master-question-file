from typing import Iterable
from myNode import Node
# Refer to README.md for the problem instructions


class SingleList():
    def __init__(self, name_list: Iterable = None) -> None:
        pass

    def remove(self, name: str) -> str or None:
        pass

    def insert(self, name: str, position: int = None):
        pass
