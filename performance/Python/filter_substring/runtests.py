import unittest
from testfile import *
import string
import copy
from random import choice, randint, shuffle

# from python.filter_substring.solution_set.filter import filter_substring


class TestFilter(unittest.TestCase):
    def setUp(self):
        self.target_substring = 'random'

        self.test_list = list()
        for _ in range(10):
            generated_string = self.generate_string(randint(5, 10))
            self.test_list.append(generated_string)

        self.solution_list = copy.copy(self.test_list)

        for _ in range(10):
            generated_string = self.generate_string(randint(5, 10))
            string_with_target = self.add_substring_to_target(generated_string, self.target_substring)

            self.test_list.append(string_with_target)

        shuffle(self.test_list)

    @staticmethod
    def generate_string(length):
        return ''.join([choice(string.ascii_lowercase) for i in range(length)])

    @staticmethod
    def add_substring_to_target(target, substring):
        return ''.join([target[:len(target)//2], substring, target[len(target)//2:]])

    def test_generated_case(self):
        self.assertEqual(set(self.solution_list), set(filter_substring(self.test_list, self.target_substring)))


if __name__ == '__main__':
    unittest.main()
