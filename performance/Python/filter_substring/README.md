# Python: Filter Substring
## KSAT List
This question is intended to evaluate the following topics:
- A0061: Create and implement functions to meet a requirement.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `filter_substring` that removes any string from a list of strings containing a specified 
sub-string.

**PARAMETERS:**
1. `list_of_strings`: A list of strings to search
2. `target`: A target string to search for in list of strings

**RETURN:** A list that contains the remaining strings that do not include `target` in the string

- If `target` is an empty string return an empty list.

### Example:

- If function is passed: ['aaaa', 'aafaceee', 'face', 'faceeee', 'bbbb'], 'face' then the function will return 
  ['aaaa', 'bbbb'].
- If the function is passed: ['abcde', 'a'], '' then the function will return [].

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
