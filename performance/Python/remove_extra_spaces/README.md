# KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0023: Declare and implement data types.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0113: Sanitize input data.
- S0082: Implement conditional control flow constructs.

# Tasks
Write a function `remove_extra_spaces` that takes a string as input and checks whether the string contains extra 
spaces in the string. If so, the function should remove the extra spaces. For example, `" Hello   World"` should be 
`"Hello World"`. Extra spaces may be in the beginning of the string, end of the string or between words. If the input 
string has no extra spaces, the function should return the string: `"The input sentence does not contain extra 
spacing"`. If the input string contains extra spaces, return a correctly formatted string. For example, when the 
string `" Welcome to   Python   Programming "` is given, the function should return `"Welcome to Python Programming"`. 
If there are no extra spaces in the provided sentence, the function will return `"The input sentence does not contain 
extra spacing"`.
