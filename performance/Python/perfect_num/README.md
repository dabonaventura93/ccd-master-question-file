# Python: Perfect Number
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement functions that can determine if a number is a perfect number.

### Task 1
Implement the function `perfect` that determines whether an input parameter is a perfect number.

**PARAMETERS:**
1. `number`: The number to verify if it is perfect or not

**RETURN:** `True` if a number is perfect and `False` if not

- Assume `number` is always an integer.

### Task 2
Implement the function `getPerfectNumbers` that determines and returns all the perfect numbers between 1 and 1000.

**PARAMETERS:**
- This function takes no parameters

**RETURNS:** A set of of all perfect numbers from 1 to 1000

- Use the function `perfect` from Task 1 to find the perfect numbers.

### Perfect Numbers
An integer number is said to be a perfect number if the sum of its factors (numbers it can evenly divide into), 
including 1 but not the number itself, is equal to the number. For example, 6 is a perfect number because 
`6 = 1 + 2 + 3` and 1, 2 and 3 all divide evenly into 6.

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
