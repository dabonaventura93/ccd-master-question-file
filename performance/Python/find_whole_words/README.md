# Python: Find Whole Words
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0024: Declare and/or implement container data type.
- S0023: Declare and implement data types.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0081: Implement a looping construct.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `findWholeWords` to search an input phrase for each word in the input word list and count the 
number of times each word appears as a whole word. Each count is stored in a dictionary, where each searched word is 
the key and the value is the number of times the word appears in the phrase. When the function is finished, it returns 
the dictionary.

**PARAMETERS:**
1. `phr`: A string containing a phrase that will have words counted from
2. `wordList`: A list of strings that need to be counted from the phrase

**RETURN:** A dictionary where the keys are the strings sought in the list, and the values are the number of times that 
key was found in the phrase

- Strings cannot be immediately preceded by or followed by an alpha or digit to match. 
- A string may or may not be preceded/followed by characters such as a space, period, commas, parenthesis etc. 
- Matching is case insensitive, so words like Cat, CAT, or CAt should still match.

### Example
If the list contains the strings 'cat' and 'dog', then the function needs to find strings that match cat and dog 
exactly. So, if the phrase is:

`A cat is in the feline category of animals. Some cats are domesticated while others are not. The Cats About Town 
(CAT) organization sponsors an initiative to free all felines. Dog is in the canine category. Some people call dogs 
doggy. I like my dog.` 

The function should find two instances of cat in the phrase as it would not match the exact string with category, 
domesticated, or cats. It would find two instances of dog as it would not match dogs or doggy.

For the phrase `cat, horse, mouse, and dog`; one instance of cat and one instance of dog exists.

Return example {'cat': 1, 'dog': 1}

## Testing
To test your code, follow the [Run Python and Networking instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).
