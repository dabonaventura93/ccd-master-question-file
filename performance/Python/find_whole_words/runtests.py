import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_find_words(self):
        test_str = 'A cat is in the feline category of animals. Some cats are domesticated while others are not. Dog is in the canine category. Some people call dogs doggy. I like my dog.'
        res = findWholeWords(test_str,['cat','dog'])
        self.assertEqual(1, res['cat'])
        self.assertEqual(2, res['dog'])
        str = 'Cat is in the feline category of animals. Some cats are domesticated while others are not. Dog is in the canine category. Some people call dogs doggy. I like my dog'
        res = findWholeWords(str,['cat','dog'])
        self.assertEqual(1, res['cat'])
        self.assertEqual(2, res['dog'])
        str = 'Cat has three letters.'
        res = findWholeWords(str,['cat','dog'])
        self.assertEqual(1, res['cat'])
        self.assertEqual(0, res['dog'])

    def test_caps(self):
        str = 'The CIA is a special organization that employes spys. Not too many people get to work for the CIA. CIA stands for Central Intelligence Agency. The National Security Agency (NSA). With help from the CIA, the NSA likes to catch the bad guys. Some say the NSA has an insatiable appetite for control.'
        res = findWholeWords(str,['cia','nsa'])
        self.assertEqual(4, res['cia'])
        self.assertEqual(3, res['nsa'])

    def test_missing_word(self):
        test_str = 'One of the words in this sentence is mispelled.'
        res = findWholeWords(test_str, ['missing'])
        self.assertEqual(0,res['missing'])
    
    def test_numbers_in_word(self):
        test_str ='2day is 2sday the day of 2. It is day2 of the work week.'
        res = findWholeWords(test_str, ['day'])
        self.assertEqual(1, res['day'])


if __name__ == '__main__':
    unittest.main()
