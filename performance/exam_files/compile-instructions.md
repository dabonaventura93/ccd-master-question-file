# Table of Contents
- [Compile Instructions](#compile-instructions)
  - [1. Build, Compile, and Run C Programming](#1-build-compile-and-run-c-programming)
    - [1.1 Use the VSCode Debugger (Easiest)](#11-use-the-vscode-debugger-easiest)
    - [1.2 Use a Workbench Quick Action](#12-use-a-workbench-quick-action)
    - [1.3 Use CMake in a VSCode terminal](#13-use-cmake-in-a-vscode-terminal)
  - [2. Run Python and Networking](#2-run-python-and-networking)
    - [2.1 Use the VSCode Debugger (Easiest)](#21-use-the-vscode-debugger-easiest)
    - [2.2 Use Python3 in VSCode terminal](#22-use-python3-in-vscode-terminal)
  - [3. Appendix](#3-appendix)
    - [3.1 C Programming Build Output](#31-c-programming-build-output)
    - [3.2 C Programming Compile Output](#32-c-programming-compile-output)
    - [3.3 C Programming Run Output](#33-c-programming-run-output)
    - [3.4 C Programming Memory Sanitizer Passed Output](#34-c-programming-memory-sanitizer-passed-output)
    - [3.5 C Programming Memory Leak Error Output](#35-c-programming-memory-leak-error-output)
    - [3.6 C Programming Buffer Overflow Error Output](#36-c-programming-buffer-overflow-error-output)
    - [3.7 Python and Networking Run Output](#37-python-and-networking-run-output)

# Compile Instructions
**While in VSCode, render this file with `ctrl+shift+V`**

\[ [TOC](#table-of-contents) \]

## 1. Build, Compile, and Run C Programming
This section will assist you with building, compiling, and running your C program.

### 1.1 Use the VSCode Debugger (Easiest)
When using the `RUN AND DEBUG` (Play) button of VS Code, your C program solution is built, compiled, and run in debug 
mode. `Debug` mode does not include memory address and leak sanitizer tests, due to an incompatibility with the 
debugger. After you are finished debugging, press `ctrl+F5` to run your code in `Release` mode. Release mode includes 
the memory address and leak sanitizer tests without the debugger.

1. Place breakpoints if needed to step through your process. Click to the left of the line numbers in `TestCode.c`
   - Keep tab focus on `TestCode.c`
2. Press `ctrl+shift+D` or click the `Run` icon on the left side of VSCode to open the debug runner
3. Click the dropdown at the top and select `Test C`
4. Press the play button
5. VSCode will automatically open a terminal and proceed to build, compile, and run your program. Example output shown 
   in [appendix 3.1](#31-c-programming-build-output), [3.2](#32-c-programming-compile-output),
   [3.3](#33-c-programming-run-output), [3.4](#34-c-programming-memory-sanitizer-passed-output), 
   [3.5](#35-c-programming-memory-leak-error-output), and [3.6](#36-c-programming-buffer-overflow-error-output).
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]

### 1.2 Use a Workbench Quick Action
1. Press `ctrl+p`
2. In the search bar that appears at the top, type `'task run'`
3. Select from:
   - `Run C Debug Tests`: To build, compile, and run your code in `Debug` mode (this does not run the memory sanitizer 
     tests)
   - `Run C Release Tests`: To build, compile, and run your code in `Release` mode (this runs the memory sanitizer 
     tests)
4. This will proceed to build, compile, and run your program. Example output shown in 
   [appendix 3.1](#31-c-programming-build-output), [3.2](#32-c-programming-compile-output),
   [3.3](#33-c-programming-run-output), [3.4](#34-c-programming-memory-sanitizer-passed-output), 
   [3.5](#35-c-programming-memory-leak-error-output), and [3.6](#36-c-programming-buffer-overflow-error-output).
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]

### 1.3 Use CMake in a VSCode terminal
1. Open a VSCode terminal with `ctrl+shift+~`
2. Change directory (`cd`) into the question folder
   - `cd C_Programming/buy_groceries`
3. Run your code using `Debug` or `Release`
   - The `Debug` mode does not run memory address and leak sanitizer tests in order to allow debugging and stepping 
     through code. The sanitizer cannot run when the debugger is in use.
   ```sh
   cmake -DCMAKE_BUILD_TYPE=Debug -B $PWD/build/debug -S $PWD && make -C $PWD/build/debug/ && ./build/debug/TestCode
   ```
   - The `Release` mode is setup to run the memory address and leak sanitizer tests. Note, if you run a debugger in 
     release mode, the sanitizer will fail to run.
   ```sh
   cmake -DCMAKE_BUILD_TYPE=Release -B $PWD/build/release -S $PWD && make -C $PWD/build/release/ && ./build/release/TestCode
   ```
4. This will proceed to build, compile, and run your program. Example output shown in 
   [appendix 3.1](#31-c-programming-build-output), [3.2](#32-c-programming-compile-output),
   [3.3](#33-c-programming-run-output), [3.4](#34-c-programming-memory-sanitizer-passed-output), 
   [3.5](#35-c-programming-memory-leak-error-output), and [3.6](#36-c-programming-buffer-overflow-error-output).
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]


## 2. Run Python and Networking

### 2.1 Use the VSCode Debugger (Easiest)
1. Place breakpoints if needed to step through your process. Click to the left of the line numbers in `testfile.py` 
   (or `client.py` for networking)
   - Keep tab focus on `testfile.py` (or `client.py` for networking)
2. Press `ctrl+shift+D` or click the `Run` icon on the left side of VSCode to open the debug runner
3. Click the dropdown at the top and select `Test Python` (or `Test Networking` if running the Networking python file)
4. Press the play button
5. VSCode will automatically open a terminal and proceed to run your program shown in 
   [appendix 3.7](#37-python-and-networking-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]

### 2.2 Use Python3 in VSCode terminal
1. Open a VSCode terminal with `ctrl+shift+~`
2. Change directory (`cd`) into the question folder
   - `cd Python/count_time`
3. Copy/Paste the following command and run it (This applies to both Python and Networking questions)
```sh
python3 runtests.py
```
4. This will proceed to run your program. Example output shown in [appendix 3.7](#37-python-and-networking-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]


## 3. Appendix
This section provides example output you can expect to see when running your code.

### 3.1 C Programming Build Output
```txt
-- The C compiler identification is GNU 9.3.0
-- The CXX compiler identification is GNU 9.3.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found PythonInterp: /usr/bin/python3.8 (found version "3.8.5") 
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found Threads: TRUE  
-- Configuring done
-- Generating done
-- Build files have been written to: C_Programming/buy_groceries/build
```

\[ [TOC](#table-of-contents) \]

### 3.2 C Programming Compile Output
```txt
Scanning dependencies of target gtest
[ 11%] Building CXX object googletest/googletest/CMakeFiles/gtest.dir/src/gtest-all.cc.o
[ 22%] Linking CXX static library ../../lib/libgtestd.a
[ 22%] Built target gtest
Scanning dependencies of target gmock
[ 33%] Building CXX object googletest/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.o
[ 44%] Linking CXX static library ../../lib/libgmockd.a
[ 44%] Built target gmock
Scanning dependencies of target gmock_main
[ 55%] Building CXX object googletest/googlemock/CMakeFiles/gmock_main.dir/src/gmock_main.cc.o
[ 66%] Linking CXX static library ../../lib/libgmock_maind.a
[ 66%] Built target gmock_main
Scanning dependencies of target TestCode
[ 88%] Building CXX object CMakeFiles/TestCode.dir/testcases.cpp.o
[ 88%] Building C object CMakeFiles/TestCode.dir/TestCode.c.o
[100%] Linking CXX executable TestCode
[100%] Built target TestCode
```

\[ [TOC](#table-of-contents) \]

### 3.3 C Programming Run Output
Will output relative to the question's unit tests.

```txt
Running main() from gmock_main.cc
[==========] Running 2 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from TestCase1
[ RUN      ] TestCase1.buyGroceriesTest_cornerCases
[       OK ] TestCase1.buyGroceriesTest_cornerCases (0 ms)
[----------] 1 test from TestCase1 (0 ms total)

[----------] 1 test from TestCase2
[ RUN      ] TestCase2.buyGroceriesTest_normalCases
[       OK ] TestCase2.buyGroceriesTest_normalCases (0 ms)
[----------] 1 test from TestCase2 (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 2 tests.
```

\[ [TOC](#table-of-contents) \]

### 3.4 C Programming Memory Sanitizer Passed Output
The following is an example of a passed memory sanitizer test; the paths were modified for brevity

```text
> Executing task: /**/C_Programming/c_circularly_linked_lists/build/release/TestCode <

Running main() from gmock_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from TestCase2
[ RUN      ] TestCase2.circular_test_normalCases
[       OK ] TestCase2.circular_test_normalCases (1 ms)
[----------] 1 test from TestCase2 (1 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (2 ms total)
[  PASSED  ] 1 test.

Terminal will be reused by tasks, press any key to close it.
```

\[ [TOC](#table-of-contents) \]

### 3.5 C Programming Memory Leak Error Output
The following is an example of a failed memory sanitizer test due to a memory leak; the paths and output were modified 
for brevity.

```text
> Executing task: /**/C_Programming/c_circularly_linked_lists/build/release/TestCode <

Running main() from gmock_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from TestCase2
[ RUN      ] TestCase2.circular_test_normalCases
[       OK ] TestCase2.circular_test_normalCases (0 ms)
[----------] 1 test from TestCase2 (3 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (5 ms total)
[  PASSED  ] 1 test.

=================================================================
==97656==ERROR: LeakSanitizer: detected memory leaks

Direct leak of 21 byte(s) in 1 object(s) allocated from:
    #0 0x7f5086a54dc6 in calloc (/lib/x86_64-linux-gnu/libasan.so.5+0x10ddc6)
    #1 0x557eff79f1fe in TestCase2_circular_test_normalCases_Test::TestBody() /**/C_Programming/c_circularly_linked_lists/testcases.cpp:56
    #2 0x557eff86021a in void testing::internal::HandleSehExceptionsInMethodIfSupported<testing::Test, void>(testing::Test*, void (testing::Test::*)(), char const*) /**/C_Programming/c_circularly_linked_lists/googletest/googletest/src/gtest.cc:2607
    ...
    #12 0x557eff7a1cce in main /**/C_Programming/c_circularly_linked_lists/googletest/googlemock/src/gmock_main.cc:70
    #13 0x7f508640e0b2 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x270b2)

SUMMARY: AddressSanitizer: 21 byte(s) leaked in 1 allocation(s).
The terminal process "bash '-c', '/**/C_Programming/c_circularly_linked_lists/build/release/TestCode'" terminated with exit code: 1.
```

\[ [TOC](#table-of-contents) \]

### 3.6 C Programming Buffer Overflow Error Output
The following is an example of a failed memory sanitizer test due to a buffer overflow; the paths and output were 
modified for brevity

```text
> Executing task: /**/C_Programming/c_circularly_linked_lists/build/release/TestCode <

Running main() from gmock_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from TestCase2
[ RUN      ] TestCase2.circular_test_normalCases
=================================================================
==98382==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7fff86767d34 at pc 0x7f1455d9657d bp 0x7fff867676f0 sp 0x7fff86766e98
WRITE of size 9 at 0x7fff86767d34 thread T0
    #0 0x7f1455d9657c  (/lib/x86_64-linux-gnu/libasan.so.5+0x9b57c)
    #1 0x55aa306732aa in TestCase2_circular_test_normalCases_Test::TestBody() /**/C_Programming/c_circularly_linked_lists/testcases.cpp:57
    #2 0x55aa307342c0 in void testing::internal::HandleSehExceptionsInMethodIfSupported<testing::Test, void>(testing::Test*, void (testing::Test::*)(), char const*) /**/C_Programming/c_circularly_linked_lists/googletest/googletest/src/gtest.cc:2607
    ...
    #13 0x7f14557c20b2 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x270b2)
    #14 0x55aa3067103d in _start (/**/C_Programming/c_circularly_linked_lists/build/release/TestCode+0x1d03d)

Address 0x7fff86767d34 is located in stack of thread T0 at offset 1540 in frame
    #0 0x55aa30671109 in TestCase2_circular_test_normalCases_Test::TestBody() /**/C_Programming/c_circularly_linked_lists/testcases.cpp:6

  This frame has 46 object(s):
    [32, 33) '<unknown>'
    [48, 49) '<unknown>'
    ...
    [1456, 1496) 'nums4' (line 28)
    [1536, 1540) 'test' (line 56) <== Memory access at offset 1540 overflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow (/lib/x86_64-linux-gnu/libasan.so.5+0x9b57c) 
Shadow bytes around the buggy address:
  0x100070ce4f50: f8 f8 f2 f2 f8 f8 f2 f2 f8 f8 f2 f2 f8 f8 f2 f2
  0x100070ce4f60: f8 f8 f2 f2 f8 f8 f2 f2 f8 f8 f2 f2 f8 f8 f2 f2
  0x100070ce4f70: f8 f8 f2 f2 00 00 04 f2 f2 f2 f2 f2 00 00 00 00
  0x100070ce4f80: f2 f2 f2 f2 00 00 00 00 f2 f2 f2 f2 00 00 00 00
  0x100070ce4f90: f2 f2 f2 f2 00 00 00 00 f2 f2 f2 f2 00 00 00 00
=>0x100070ce4fa0: 00 f2 f2 f2 f2 f2[04]f3 f3 f3 00 00 00 00 00 00
  0x100070ce4fb0: 00 00 00 00 00 00 00 00 00 00 f1 f1 f1 f1 00 00
  0x100070ce4fc0: f3 f3 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100070ce4fd0: 00 00 00 00 f1 f1 f1 f1 00 00 f2 f2 00 00 00 00
  0x100070ce4fe0: f2 f2 f2 f2 00 00 00 00 f3 f3 f3 f3 00 00 00 00
  0x100070ce4ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==98382==ABORTING
The terminal process "bash '-c', '/**/C_Programming/c_circularly_linked_lists/build/release/TestCode'" terminated with exit code: 1.
```

\[ [TOC](#table-of-contents) \]

### 3.7 Python and Networking Run Output
```txt
Running tests...
----------------------------------------------------------------------
..
----------------------------------------------------------------------
Ran 2 tests in 0.001s
OK
```

\[ [TOC](#table-of-contents) \]
