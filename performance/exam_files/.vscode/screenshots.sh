#!/bin/bash

ARG1=${1:-.}

for d in $ARG1/C_Programming/*/ ; do
    cd $d
    pwd
    cmake -DCMAKE_BUILD_TYPE=release -B .build/release/ -S .
    make -C .build/release/ &> screenshots.txt
    script -q -c ".build/release/TestCode"
    cat typescript >> screenshots.txt
    rm typescript
    cd -
done

for d in $ARG1/Python/*/ ; do
    cd $d
    pwd
    python3 runtests.py -v &> screenshots.txt
    cd -
done

for d in $ARG1/Networking/*/ ; do
    cd $d
    pwd
    python3 runtests.py -v &> screenshots.txt
    cd -
done
