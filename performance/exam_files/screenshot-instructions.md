# Table of Contents
- [Screenshot Instructions](#screenshot-instructions)
  - [1. Creating & Saving Screenshots](#1-creating-saving-screenshots)
  - [2. Appendix](#2-appendix)
    - [2.1 C Programming Run Output](#21-c-programming-run-output)
    - [2.2 Python and Networking Run Output](#22-python-and-networking-run-output)

# Screenshot Instructions
**While in VSCode, render this file with `ctrl+shift+V`** 

This provides guidance on creating and saving screenshots of your code's output to your exam repository.

\[ [TOC](#table-of-contents) \]

## 1. Creating & Saving Screenshots
1. Using your OS's screenshot application, create an image of your question's terminal output; it should resemble 
   [appendix 2.1](#21-c-programming-run-output), for c questions, and [2.2](#22-python-and-networking-run-output), for 
   Python & Networking questions.
   - Include the `question's name` in your image's file name.
   - When you save the file, take note of the location.
2. To add the image to your VS Code Server's browser view, simply drag your image to the `question's folder`.

\[ [TOC](#table-of-contents) \]

## 2. Appendix
The appendix provides samples of what your screenshots should capture; note, your output may contain failure 
statements depending on your solution.

\[ [TOC](#table-of-contents) \]

### 2.1 C Programming Run Output
Will output relative to the question's unit tests.
```txt
Running main() from gmock_main.cc
[==========] Running 2 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from TestCase1
[ RUN      ] TestCase1.buyGroceriesTest_cornerCases
[       OK ] TestCase1.buyGroceriesTest_cornerCases (0 ms)
[----------] 1 test from TestCase1 (0 ms total)

[----------] 1 test from TestCase2
[ RUN      ] TestCase2.buyGroceriesTest_normalCases
[       OK ] TestCase2.buyGroceriesTest_normalCases (0 ms)
[----------] 1 test from TestCase2 (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 2 tests.
```

\[ [TOC](#table-of-contents) \]

### 2.2 Python and Networking Run Output
```txt
Running tests...
----------------------------------------------------------------------
..
----------------------------------------------------------------------
Ran 2 tests in 0.001s
OK
```

\[ [TOC](#table-of-contents) \]
