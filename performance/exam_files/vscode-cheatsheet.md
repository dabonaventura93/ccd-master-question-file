# VSCode Cheatsheet
**While in VSCode, render this file with `ctrl+shift+V`**

- Open a terminal: `ctrl+shift+~`
- Open debugger tab: `ctrl+shift+D`
- Render markdown file: `ctrl+shift+V` (while focus on file)
- Comment line: `ctrl+/` (while cursor on line)
- Open File: `ctrl+O`
- Open Folder: `ctrl+K` then `ctrl+O`
- Run a task (as configured in `.vscode/tasks.json`):
  - `ctrl+p`
  - In the search bar that appears, type `'task '` (DON'T include quotes but DO include the trailing space)
  - Select the desired task to run

**Custom keyboard shortcuts**
These short cuts must be run from the intended question's folder. Simply select a file within the question's folder 
and use these key bindings. Note, the selected file must be within the question's root folder, not sub folders.

- Run C Release Tests: `ctrl+F5`
- Remove the build folder: `ctrl+shift+F4`

**Open keyboard shortcuts**
1. press `ctrl+shift+P`
2. type `keyboard shortcuts`
3. hit enter on `Preferences: Open Keyboard Shortcuts`

