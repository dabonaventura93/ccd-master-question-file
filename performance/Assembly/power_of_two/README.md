# KSAT List
This question is intended to evaluate the following topics:
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0117: Utilize labels for control flow.
- S0125: Utilize general purpose instructions.
- S0126: Utilize arithmetic instructions.
- S0118: Implement conditional control flow.
- S0136: Utilize x86 calling conventions
- S0134: Utilize registers and sub-registers
- S0177: Write assembly code that accomplishes the same functionality as a given a high level language (C or Python) 
         code snippet.

# Tasks
Create code that calculates the power of 2, given a number 'x', and returns the result 'z'.
```text
2 ^ x = z
2 ^ 2 = 4
2 ^ 3 = 8
```
