# KSAT List
This question is intended to evaluate the following topics:
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0126: Utilize arithmetic instructions.
- S0136: Utilize x86 calling conventions
- S0134: Utilize registers and sub-registers

# Tasks
Given three 32 bit integer arrays of 4 elements each, add each element of the first (x) to the corresponding element 
of the second (y) (basically, like adding one vector to another), and store the result in the third (z). - Ex. (in C):

```c
z[i] = x[i] + y[i];
```
