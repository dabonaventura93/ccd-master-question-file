#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include "test_defs.h"
#define MAX 1024 


extern char* character_replace(char*, char*, char, char);


#define RUN_TEST(input, check, oldChar, newChar) {\
    size_t oldEsp = 0; \
    size_t newEsp = 0; \
    size_t retreived_ebp = 0; \
    char buf[MAX] = { '\0' }; \
\
    __asm { mov oldEsp, esp }; \
    char* result = character_replace((input), buf, (oldChar), (newChar)); \
    if (strcmp(check, result) != 0) \
        printf("Expected: %s, Got: %s\n", check, result); \
    ASSERT_NEQUAL(input, result); \
    ASSERT_EQUAL(strcmp(check, result), 0); \
    __asm { mov newEsp, esp } \
\
    if (oldEsp != newEsp) \
        FAIL_WITH_CODE(AssertFailStackUnbalanced); \
}


// Tests assembly that replaces all instances of a character in a string with another character.
TEST(CharacterReplace, character_replace)
{
    RUN_TEST("Hell0 W0rld!", "Hello World!", '0', 'o');
    RUN_TEST("A longer strIng with more characters In it. For this one replace all the Instances of I with i.", "A longer string with more characters in it. For this one replace all the instances of i with i.", 'I', 'i')
    RUN_TEST("1O1OO111OO1", "10100111001", 'O', '0');
    RUN_TEST("babaabbbbbaabaaaaabbbbbbbbababababaaaaaaaaaaababbbbbbbb", "bAbAAbbbbbAAbAAAAAbbbbbbbbAbAbAbAbAAAAAAAAAAAbAbbbbbbbb", 'a', 'A');
}
