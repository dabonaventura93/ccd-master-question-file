# KSAT List
This question is intended to evaluate the following topics:
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0117: Utilize labels for control flow.
- S0125: Utilize general purpose instructions.
- S0118: Implement conditional control flow.
- S0136: Utilize x86 calling conventions
- S0134: Utilize registers and sub-registers
- S0177: Write assembly code that accomplishes the same functionality as given in a high level language (C or Python) 
         code snippet.

# Tasks
This task is similar to memcopy but with one important exception: it must 
be able to handle overlapped memory copies (just like memmove). That is to say, it has to be able
to perform a copy where the source and destination overlap. Assume that src < dest, 
in which case you must either copy in reverse or use a buffer.
