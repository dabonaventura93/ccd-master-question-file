#include <Windows.h>
#include <stdio.h>
#include "test_defs.h"

extern void movememory(void* dst, void* src, size_t count);

TEST(ThirdTask, movememory)
{
    size_t oldEsp = 0;
    size_t newEsp = 0;
    unsigned char buffer1[MAX_PATH + 1] = { 0 };
    unsigned char buffer2[MAX_PATH + 1] = { 0 };

    memset(buffer1, 0x42, 130);
    memset((buffer1 + 130), 0x41, 130);

    memset(buffer2, 0x42, 70);
    memset((buffer2 + 70), 0x41, 190);

    __asm { mov oldEsp, esp }
    movememory((buffer1 + 70), (buffer1 + 130), 100);
    __asm { mov newEsp, esp }


    if (oldEsp != newEsp)
        FAIL_WITH_CODE(AssertFailStackUnbalanced);

    ASSERT_EQUAL(0, memcmp(buffer1, buffer2, MAX_PATH));

}