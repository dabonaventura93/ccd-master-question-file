#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include "test_defs.h"
#define MAX 25


extern void counter(char*, char);





// Tests assembly that returns the occurrences of a specific character.
TEST(Counter, counter)
{
    size_t oldEsp = 0;
    size_t newEsp = 0;
    size_t retreived_ebp = 0;

    // input
    CONST char* x = "The C language combines all of the power of assembly language with all of the ease of use of assembly language. -Mark Pearce\0"; // Original String
    CONST char a = 'a';

    int count = 0;
    for (int i = 0; i < strlen(x); i++){
        if (x[i] == a) {
            count++;
        }
    }

    __asm { mov oldEsp, esp };

    // Call assembly and then test return value
    counter(x, 'a');

    // Get result from assembly
    char* copy_eax = 0;
    __asm { mov copy_eax, eax }; // Return value in EAX

    // Check answer
    ASSERT_EQUAL(count, copy_eax);

    __asm { mov newEsp, esp }

    if (oldEsp != newEsp)
        FAIL_WITH_CODE(AssertFailStackUnbalanced);
}
