bits 32

section .text

global _reverse_string
global _reverse_increment
global _reverse_length_loop
global _reverse_put_char
global _reverse_char_loop

; Refer to README.md for the problem instructions
;
; void __cdecl reverse_string(int* x)
_reverse_string:
    push ebp
    mov ebp, esp
    sub esp, 10h        ; space

    ; Stack addresses of interest
    ; ebp + 0x08 original (input) string
    ; ebp + 0x0C reversed (output) string

    ;;;;;;;;;;;;
    ;code begin

    

    ;code end
    ;;;;;;;;;;;;

    mov eax, [ebp+0x0C]    ; output string

    add esp, 10h        ; remove space
    mov esp, ebp
    pop ebp

    ret
