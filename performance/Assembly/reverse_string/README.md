# KSAT List
This question is intended to evaluate the following topics:
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0117: Utilize labels for control flow.
- S0125: Utilize general purpose instructions.
- S0118: Implement conditional control flow.
- S0136: Utilize x86 calling conventions
- S0134: Utilize registers and sub-registers
- S0177: Write assembly code that accomplishes the same functionality as a given a high level language (C or Python) 
         code snippet.

# Tasks
Given an input string, reverse the string and store it in the output string buffer.
