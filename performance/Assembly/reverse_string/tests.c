#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include "test_defs.h"
#define MAX 25


extern void reverse_string(char*, char*);




// Tests assembly that takes a string and reverses the order of its characters.
TEST(ReverseString, reverse_string)
{
    size_t oldEsp = 0;
    size_t newEsp = 0;
    size_t retreived_ebp = 0;

    // input
    CONST char original[9] = "stressed\0"; // Original String
    CONST char answer[9] = "desserts\0"; // Original String
    char solution[9];

    __asm { mov oldEsp, esp };

    // Call simple add assembly and then test return value
    reverse_string(original, solution);
    int copy_eax = 0;
    __asm { mov copy_eax, eax }; // Return value in EAX

    ASSERT_EQUAL(strcmp(answer, solution), 0);

    __asm { mov newEsp, esp }

    if (oldEsp != newEsp)
        FAIL_WITH_CODE(AssertFailStackUnbalanced);
}