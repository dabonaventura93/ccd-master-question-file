#include "stack.h"

#include <stdlib.h>
#include <string.h>


/* 
 * This function will take a value and push it onto the stack
 * @param stack_ptr a pointer to the stack that you will push elements onto.
 * @param stored_value a pointer to the value in a tree's node.
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int push(p_stack stack_ptr, void *stored_value) {

    return 0;
}

/* 
 * This function will take pop the top element off of the stack and assign the value coming off to item_value_ptr
 * @param stack_ptr a pointer to the stack that you will pop elements from.
 * @param item_value_ptr a pointer to a tree's node.
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int pop(p_stack stack_ptr, void **item_value_ptr) {

    return 0;
}

/* 
 * This function will allocate a new stack.
 * @return returns a new stack
 */
p_stack stack_create() {
    
    p_stack const new_stack = NULL;
    return new_stack;
}

/* 
 * This function will pop and free all elements of the stack
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int stack_destroy(p_stack const stack_ptr) {

    return 0;
}
