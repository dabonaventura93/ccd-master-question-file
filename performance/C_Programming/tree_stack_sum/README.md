# C Programming: Tree Stack Sum
## KSAT List
This question is intended to evaluate the following topics:
- A0061: Create and implement functions to meet a requirement.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0397: Create and use complex Data Structures.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0066: Add and remove nodes from a Linked List.
- S0052: Implement a function that returns a single value.
- S0078: Push and pop a Stack.
- S0062: Create and destroy a Stack.
- S0055: Create and destroy a Linked List.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.

## Tasks
These tasks will perform calculations using binary tree and stack structure operations.

### Task 1
Implement the `push` function, in `stack.c`, so it takes a value, pushes it onto the stack, and returns a status code.

**PARAMETERS:**
- `stack_ptr` a pointer to a `Stack_` struct (the stack) that you will push elements onto
- `stored_value` a pointer to void containing a tree node's value (within the `_Node` struct in `binary_tree.h`)

**RETURN:** an int of: `STACK_OPERATION_SUCCESS` (0) on success; or `STACK_OPERATION_ERROR` (-1) if `stack_ptr` or `stored_value` are null, or memory is unavailable

### Task 2
Implement the `pop` function, in `stack.c`, so it takes the top element off of the stack, stores the value in `item_value_ptr`, and returns a status code.

**PARAMETERS:**
- `stack_ptr` a pointer to a `Stack_` struct (the stack) that you will pop elements from
- `item_value_ptr` a double pointer to void containing a tree node's value (within the `_Node` struct in 
  `binary_tree.h`)

**RETURN:** an int of: `STACK_OPERATION_SUCCESS` (0) on success; or `STACK_OPERATION_ERROR` (-1) if `stack_ptr` or `item_value_ptr` are null

### Task 3
Implement the `stack_create` function, in `stack.c`, so it creates and returns a new stack.

**PARAMETERS:**
- There are no parameters to this function

**RETURN:** a pointer to a `Stack_` struct (the stack) representing the top element

### Task 4
Implement the `stack_destroy` function, in `stack.c`, so it pops and frees all elements of the stack and returns a 
status code.

**PARAMETERS:**
- `stack_ptr` a pointer to a `Stack_` struct (the stack) that you will pop elements from

**RETURN:** an int of: `STACK_OPERATION_SUCCESS` (0) on success; or `STACK_OPERATION_ERROR` (-1) if `stack_ptr` is null

### Task 5
Implement a function `calculate_tree_sum` that calculates the sum of the elements within the tree by traversing the nodes and putting them into a stack. When finished, `calculate_tree_sum` returns the sum of all nodes in the tree.

**PARAMETERS:**
- `tree_ptr` a pointer to a `_Tree` struct defined in `binary_tree.h`

**RETURN:** an int containing the sum of all nodes in the tree, or `TREE_ERROR` if `tree_ptr` is null, the tree's root 
is null, or the stack cannot be created

#### Example 1
![Example 1](resources/test_tree_one_graphic.PNG)

#### Example 2
![Example 2](resources/test_tree_two_graphic.PNG)

#### Example 3
![Example 3](resources/test_tree_three_graphic.PNG)

#### Example 4
![Example 4](resources/test_tree_four_graphic.PNG)

## Important Notes
You can find the stack function signatures in `stack.h`. You are allowed to modify these if you want to implement 
these differently.

***IMPORTANT: You are not allowed to use recursion and you may not traverse the stack that is used in the tree 
structure.***

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
