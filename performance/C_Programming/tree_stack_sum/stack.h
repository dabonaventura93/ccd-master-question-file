#pragma once

typedef struct StackElem_ {
    struct StackElem_ *next;
    void *stored_item;
} stack_elem, *p_stack_elem;

typedef struct Stack_ {
    p_stack_elem top;
    unsigned int size;
} stack, *p_stack;

#ifdef __cplusplus
extern "C" {
#endif

int push(p_stack stack_ptr, void *stored_value);
int pop(p_stack stack_ptr, void **item_value_ptr); 

p_stack stack_create();
int stack_destroy(p_stack stack_ptr);

#ifdef __cplusplus
}
#endif

#define STACK_OPERATION_SUCCESS (0)
#define STACK_OPERATION_ERROR (-1)

#define STACK_IS_EMPTY(stack_ptr) ((stack_ptr)->size == 0)
