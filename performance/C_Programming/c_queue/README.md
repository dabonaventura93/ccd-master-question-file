# C Programming: C Queue
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0058: Create and destroy a Queue.
- S0071: Enqueue and dequeue a Queue.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

## Tasks
Implement a simple queue using a linked list. A `numNode` struct is defined in the `TestCode.h` file to 
facilitate the creation of the queue.

### Task 1
Implement an `enqueue` function that adds an item to the queue and returns a status code.

**PARAMETERS:**
1. `end`: a pointer to a `struct numNode` pointer representing the end of the queue. Before this function returns, 
   the `end` pointer is updated so it points to the new last item in the queue.
2. `data`: an integer representing the new data to be added to the queue.

**RETURN:** an int containing `0` on success, or `1` if `end` is null or memory is unavailable

### Task 2
Implement a `dequeue` function that removes the first item in the queue and returns the data stored in that item.

**PARAMETERS:**
1. `front`: a pointer to a `struct numNode` pointer representing the front of the queue. 

**RETURN:** an int containing `0` if `front` was null; otherwise, it is the data within the removed node

- Before this function returns, the `front` pointer is updated so it points to the next item in the queue.
- If there are no remaining items, set `front` to NULL.

### Task 3
Implement an `emptyQueue` function that removes all items in the queue.

**PARAMETERS:**
1. `front`: a pointer to a `struct numNode` pointer representing the front of the queue. 

**RETURN:** void

- Ensure all items in the queue are properly freed.
- Ensure the `front` parameter is not null; if so, it should return. 

## Task 4
Implement a function `makeQueue` that creates a queue based off a series of commands and data, and returns the front of 
the queue when finished.

**PARAMETERS:**
1. `actions`: an int array - this array contains values of 1, 2, or 3 in the even indices (0,2,4 etc.) that indicate 
   which type of action to do with the queue:

   1. dequeue (remove an item from the queue)
   2. enqueue (add an item to the queue)
   3. empty queue

   - The odd indices will only apply for enqueue actions. If the action received is action enqueue (2), then the next 
     index contains the value to add to the queue. The odd indices have no effect on dequeue (1) or empty queue (3)
        
   - So if the function receives:  [2, 7, 2, 2, 1, 0, 3, 0], then you would enqueue 7 to the queue, then enqueue 2 to 
     the queue, then remove an item from the queue, then empty the queue.

2. `numActions`: an int representing the total number of actions supplied in the array. In the above example this would 
   be 4.

**RETURN:** `struct numNode` pointer that represents the front of the queue or `null` if any action is not 1 - 3.

- The function should iterate the actions array and determine what actions should occur on the queue. 
- Depending on the action, the appropriate function should be called (i.e. `enqueue`, `dequeue`, or `emptyQueue`). 
- Assume that the array will always have an even number of elements. 

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.

