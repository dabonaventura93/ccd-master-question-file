#include <gmock/gmock.h>
#include "TestCode.h"

TEST(Enqueue_Tests, zeroCase)
{
    int data = 0;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    ASSERT_FALSE(NULL == end);
    ASSERT_EQ(data, end->num);
    ASSERT_TRUE(NULL == end->next);
    free(end);
    end = NULL;
}

TEST(Enqueue_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    ASSERT_FALSE(NULL == end);
    ASSERT_EQ(data, end->num);
    ASSERT_TRUE(NULL == end->next);
    free(end);
    end = NULL;
}

TEST(Enqueue_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    ASSERT_FALSE(NULL == end);
    ASSERT_EQ(data, end->num);
    ASSERT_TRUE(NULL == end->next);
    free(end);
    end = NULL;
}

TEST(Enqueue_Tests, nullCase)
{
    int data = 7734;
    ASSERT_EQ(1, enqueue(NULL, data));
}

TEST(Dequeue_Tests, zeroCase)
{
    int data = 0;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    struct numNode *front = end;
    ASSERT_FALSE(NULL == front);
    ASSERT_FALSE(NULL == end);
    ASSERT_TRUE(front == end);
    ASSERT_EQ(data, dequeue(&front));
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(Dequeue_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    struct numNode *front = end;
    ASSERT_FALSE(NULL == front);
    ASSERT_FALSE(NULL == end);
    ASSERT_TRUE(front == end);
    ASSERT_EQ(data, dequeue(&front));
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(Dequeue_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    struct numNode *front = end;
    ASSERT_FALSE(NULL == front);
    ASSERT_FALSE(NULL == end);
    ASSERT_TRUE(front == end);
    ASSERT_EQ(data, dequeue(&front));
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(Dequeue_Tests, nullCase1)
{
    ASSERT_EQ(0, dequeue(NULL));
}

TEST(Dequeue_Tests, nullCase2)
{
    struct numNode *front = NULL;
    ASSERT_EQ(0, dequeue(&front));
}

TEST(EmptyQueue_Tests, oneNodeCase)
{
    int data = 7;
    struct numNode *end = NULL;
    ASSERT_EQ(0, enqueue(&end, data));
    struct numNode *front = end;
    ASSERT_FALSE(NULL == end);
    ASSERT_EQ(data, end->num);
    ASSERT_TRUE(NULL == end->next);
    ASSERT_FALSE(NULL == front);
    ASSERT_EQ(data, front->num);
    ASSERT_TRUE(NULL == front->next);

    emptyQueue(&front);
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(EmptyQueue_Tests, twoNodesCase)
{
    int data[] = {1, 3};
    int sz_data = 2;
    struct numNode *end = NULL;
    struct numNode *front = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        ASSERT_EQ(0, enqueue(&end, data[i]));
        ASSERT_FALSE(NULL == end);
        ASSERT_EQ(data[i], end->num);
        ASSERT_TRUE(NULL == end->next);
        if (i == 0)
        {
            front = end;
            ASSERT_EQ(data[i], front->num);
            ASSERT_TRUE(NULL == front->next);
        }
        else
        {
            ASSERT_TRUE(front->next == end);
        }
    }
    emptyQueue(&front);
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(EmptyQueue_Tests, threeNodesCase)
{
    int data[] = {1, 3, 6};
    int sz_data = 3;
    struct numNode *end = NULL;
    struct numNode *front = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        ASSERT_EQ(0, enqueue(&end, data[i]));
        ASSERT_FALSE(NULL == end);
        ASSERT_EQ(data[i], end->num);
        ASSERT_TRUE(NULL == end->next);
        if (i == 0)
        {
            front = end;
            ASSERT_EQ(data[i], front->num);
            ASSERT_TRUE(NULL == front->next);
        }
        else if (i == 1)
        {
            ASSERT_TRUE(front->next == end);
        }
        else
        {
            ASSERT_FALSE(front->next == end);
        }
    }
    emptyQueue(&front);
    ASSERT_TRUE(NULL == front);
    end = NULL;
}

TEST(MakeQueue_Tests, enqueueCase1)
{

    int actions[] = { 2, 4, 2, 5, 2, 10 };
    struct numNode *head = makeQueue(actions, 3);
    struct numNode *res = head;
    int test[] = { 4, 5, 10 };
    ASSERT_FALSE(NULL == res); //res should not be NULL
    for (int i = 0; res != NULL; i++, res = res->next)
    {
        ASSERT_EQ(test[i], res->num);
    }
    ASSERT_TRUE(NULL == res);
    // Ensure memory is freed
    emptyQueue(&head);
    ASSERT_TRUE(NULL == head);
}

TEST(MakeQueue_Tests, enqueueEmptyQueueCase1)
{
    int actions[] = { 2, 4, 2, 5, 2, 10, 3, 0 };
    ASSERT_TRUE(NULL == makeQueue(actions, 4));
}

TEST(MakeQueue_Tests, enqueueDequeueCase1)
{
    int actions[] = { 2, 7, 2, 4, 2, 5, 1, 0, 2, 10, 1, 0 };
    struct numNode *head = makeQueue(actions, 6);
    struct numNode *res = head;
    ASSERT_FALSE(NULL == res); //res should not be NULL
    int test2[] = { 5, 10 };
    for (int i = 0; res != NULL; i++, res = res->next)
    {
        ASSERT_EQ(test2[i], res->num);
    }
    ASSERT_TRUE(NULL == res); //res should now be NULL
    // Ensure memory is freed
    emptyQueue(&head);
    ASSERT_TRUE(NULL == head);
}

TEST(MakeQueue_Tests, enqueueDequeueCase2)
{
    int actions[] = { 2, 4, 2, 5, 2, 10, 1, 0, 1, 0, 1, 0 };
    ASSERT_EQ(NULL, makeQueue(actions, 6));
}

TEST(MakeQueue_Tests, enqueueDequeueCase3)
{
    int actions5[] = { 2, 4, 2, 5, 2, 10, 1, 0, 1, 0, 1, 0, 2, 50 };
    struct numNode *head = makeQueue(actions5, 7);
    struct numNode *res = head;
    ASSERT_FALSE(NULL == res); //res should not be NULL
    int test3[] = { 50 };
    for (int i = 0; res != NULL; i++, res = res->next)
    {
        ASSERT_EQ(test3[i], res->num);
    }
    ASSERT_TRUE(NULL == res); //res should now be NULL
    // Ensure memory is freed
    emptyQueue(&head);
    ASSERT_TRUE(NULL == head);
}
