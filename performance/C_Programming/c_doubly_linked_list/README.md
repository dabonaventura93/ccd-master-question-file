# C Programming: C Doubly Linked List
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0050: Implement a function that doesn't return a value.
- S0067: Find an item in a Doubly Linked List.
- S0068: Add and remove nodes from a Doubly Linked List.
- S0052: Implement a function that returns a single value.
- S0056: Create and destroy a Doubly Linked List.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
    
## Tasks
These tasks allow you to create and work with a doubly linked list.

### Task 1
Implement the function `buildList` that creates a doubly linked list and returns the head of the list.

**Parameters:**
1. `names`: an array of char * of people's first names
2. `size`: an int representing the number of elements in the array

**Return:** struct `nameNode` pointer to the head of the doubly-linked list, or `null` if `names` is null or contains 
null elements

- The `buildList` function should iterate through the `names` array and create a doubly-linked list, creating a node for 
  each name in the `names` array. 
- Use the `nameNode` struct defined in the `TestCode.h` file to create the doubly-linked list. 
- Each name is added to the head of the doubly-linked list. 
- Empty strings should be ignored. 

### Task 2
Implement the function `removeNode` that will remove a node based on a given name and return the head of the list.

**Parameters:**
1. `head`: a pointer to the first node in the doubly-linked list created in Task 1
2. `findName`: a char * of a person's name that may be in a linked-list

**Return:** struct `nameNode` pointer to the head of the doubly-linked list

- The `removeNode` function will search the doubly-linked list for the name in `findName`. 
- If the name is found, the node containing the name must be removed from the doubly-linked list, ensure to cleanup 
  any memory.
- If the name is not found, do nothing.

### Task 3
Implement the function `freeMemory` that frees all the memory in a doubly-linked list.

**Parameters:**
1. `head`: a pointer to the first node in the doubly-linked list to be deleted

**Return:** void

- `freeMemory` receives a pointer to your linked-list's head node so you can iterate over the linked list and free all 
  allocated memory.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
