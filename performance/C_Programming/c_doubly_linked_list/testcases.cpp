#include <gmock/gmock.h>
#include "TestCode.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


const char *names[] = { "Joe","Ace","Gene","Paul","Peter","Hank","Timmy","Sarah","Alice","Carol" };
// The reverse of names
const char *names2[] = { "Carol","Alice","Sarah","Timmy","Hank","Peter","Paul","Gene","Ace","Joe" };
const char *names3[] = { "Joe","Ace","Gene","Paul","Hank","Timmy","Sarah","Alice","Carol" };
// The reverse of names3
const char *names4[] = { "Carol","Alice","Sarah","Timmy","Hank","Paul","Gene","Ace","Joe" };
const char *names5[] = { "Ace","Gene","Paul","Hank","Timmy","Sarah","Alice","Carol" };
// The reverse of names5
const char *names6[] = { "Carol","Alice","Sarah","Timmy","Hank","Paul","Gene","Ace" };
const char *names7[] = { "Ace","Gene","Paul","Hank","Timmy","Sarah","Alice" };
// The reverse of names7
const char *names8[] = { "Alice","Sarah","Timmy","Hank","Paul","Gene","Ace" };
const char *names9[] = { "Bobby" };

TEST(BuildList_Tests, checkDoubleList)
{
    // Keep in mind the list will be in reverse order
    nameNode *head = buildList(names, sizeof(names) / sizeof(*names));
    nameNode *curr = head;
    nameNode *prev = head;
    int i = 0;

    while (curr != NULL)
    { // Check to see if all names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names2[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }
    // Checks to see if the list is the same size as the array
    ASSERT_EQ(sizeof(names) / sizeof(*names), i);

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);

    head = buildList(names3, sizeof(names3) / sizeof(*names3));
    curr = head;
    prev = head;
    i = 0;

    while (curr != NULL)
    { // Check to see if all names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names4[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }
    // Checks to see if the list is the same size as the array
    ASSERT_EQ(sizeof(names3) / sizeof(*names3), i);

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names3[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);
}

TEST(BuildList_Tests, cornerCases)
{
    const char *namesSomeEmpty[] = {"", "Alice", "Sarah", "Timmy", "", "Peter", "Paul", "Gene", "Ace", ""};
    const char *reversedNamesSomeEmpty[] = {"Ace", "Gene", "Paul", "Peter", "Timmy", "Sarah", "Alice"};
    const char *forwardNamesSomeEmpty[] = {"Alice", "Sarah", "Timmy", "Peter", "Paul", "Gene", "Ace"};
    nameNode *head = buildList(namesSomeEmpty, sizeof(namesSomeEmpty) / sizeof(*namesSomeEmpty));
    nameNode *curr = head;
    nameNode *prev = head;
    int i = 0;

    while (curr != NULL)
    { // Check to see if all names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, reversedNamesSomeEmpty[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }
    // Checks to see if the list is the same size as the array
    ASSERT_EQ(sizeof(reversedNamesSomeEmpty) / sizeof(*reversedNamesSomeEmpty), i);

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, forwardNamesSomeEmpty[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);
}

TEST(BuildList_Tests, nullCases)
{
    const char *namesNull[] = {NULL};
    const char *namesEmpty[] = {"", "", ""};
    ASSERT_TRUE(NULL == buildList(NULL, 0));
    ASSERT_TRUE(NULL == buildList(namesNull, 1));
    ASSERT_TRUE(NULL == buildList(namesEmpty, 3));
}

TEST(RemoveNode_Tests, checkRemoveCenterNode)
{
    // Keep in mind the list will be in reverse order of the array
    nameNode *head = buildList(names, sizeof(names) / sizeof(*names));
    int i = 0;
    head = removeNode(head, "Peter");
    nameNode *curr = head;
    nameNode *prev = head;

    while (curr != NULL)
    { // Check to see if the expected names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names4[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names3[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);
}

TEST(RemoveNode_Tests, checkRemoveTailNode)
{
    // Keep in mind the list will be in reverse order of the array
    nameNode *head = buildList(names3, sizeof(names3) / sizeof(*names3));
    int i = 0;

    head = removeNode(head, "Peter");
    nameNode *curr = head;
    nameNode *prev = head;

    while (curr != NULL)
    { // Check to see if the expected names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names4[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names3[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    head = removeNode(head, "Joe");
    curr = head;
    prev = head;

    i = 0;
    while (curr != NULL)
    { // Check to see if the expected names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names6[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names5[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);
}

TEST(RemoveNode_Tests, checkRemoveHeadNode)
{
    // Keep in mind the list will be in reverse order of the array
    nameNode *head = buildList(names5, sizeof(names5) / sizeof(*names5));
    int i = 0;

    head = removeNode(head, "Carol");
    nameNode *curr = head;
    nameNode *prev = head;

    while (curr != NULL)
    { // Check to see if the expected names in the list are present and point to the next expected name
        ASSERT_EQ(0, strcmp(curr->name, names8[i]));
        prev = curr;
        curr = curr->next;
        i++;
    }

    ASSERT_TRUE(NULL == curr);

    i = 0;
    while (prev != NULL)
    { // Check to see if the expected names in the list are present and point to the previous expected name
        ASSERT_EQ(0, strcmp(prev->name, names7[i]));
        prev = prev->prev;
        i++;
    }

    ASSERT_TRUE(NULL == prev);

    freeMemory(head);
}

TEST(RemoveNode_Tests, checkRemoveOneItemList)
{
    nameNode *head = buildList(names9, sizeof(names9) / sizeof(*names9));
    // Pass a name that's not in the list
    head = removeNode(head, "Carol");
    ASSERT_EQ("Bobby", head->name);
    ASSERT_EQ(NULL, head->next);
    ASSERT_EQ(NULL, head->prev);

    // Pass a name that's in the list
    head = removeNode(head, "Bobby");
    ASSERT_EQ(NULL, head);
    // Determine if head isn't null
    if (head != NULL)
        freeMemory(head);
}
