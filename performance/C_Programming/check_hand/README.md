# C Programming: Check Hand
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement the function `checkHand` that determines the best type of hand in a set of poker cards and returns the best 
hand.

**PARAMETERS:**
1. `hand`: a 5 element integer array that contains the poker hand

**RETURN:** character pointer to an array containing the best hand, as explained below

- The valid values for this array are the numbers 1 through 13.
- There are only three types of hands worth noting: three of a kind, four of a kind, or a straight.
  - A straight involves all 5 cards being in a sequence such as 3, 4, 5, 6, 7 etc. 
  - A four of a kind and a three of a kind involves 4 of the cards and 3 of the cards being the same respectively.
- Evaluate the array to determine the best hand. The precedence of hands is:
  1. straight (best)
  2. four of a kind
  3. three of a kind
  4. nothing (worst)
- Return one of the following strings based on analysis: `straight`, `four`, `three`, `nothing`, or `invalid`. 
- Return `invalid` if any of the values are < 1 or > 13 or if the input array is NULL.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
