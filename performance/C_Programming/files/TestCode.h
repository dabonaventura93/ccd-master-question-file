#pragma once

#include <stdio.h>
#define ERROR_SUCCESS 0
#define ERROR_FILE_INVALID 1006
#define ERROR_INVALID_DATA 13

#ifdef __cplusplus
extern "C" {
#endif
    int findStats(int state, int stats[]);

#ifdef __cplusplus
}
#endif