#include <gmock/gmock.h>
#include <string.h>
#include "TestCode.h"


TEST(GeneralTree_Tests, normalCases)
{
    int nums[] = {0,4,4,2,4,5,4,6,0,12,0,14,14,77,14,56};
    struct TreeNode *root = buildTree(nums, sizeof(nums) / sizeof(int));
    struct TreeNode *temp = root;
    int parent = 14;
    int child = 77;

    while(temp != NULL && temp->number != parent)
        temp = temp->nextSibling;

    if(temp == NULL) // parent not found
        FAIL() << "Parent not found";
    else  // parent found
    {
        temp = temp->firstChild;
        while(temp != NULL && temp->number != child)
            temp = temp->nextSibling;

        if(temp == NULL)  //child not found
            FAIL() << "Child not found";
        else
            ASSERT_EQ(temp->number, child);
    }

    temp = root;
    parent = 4;
    child = 6;

    while(temp != NULL && temp->number != parent)
        temp = temp->nextSibling;

    if(temp == NULL) // parent not found
        FAIL() << "Parent not found";
    else  // parent found
    {
        temp = temp->firstChild;
        while(temp != NULL && temp->number != child)
            temp = temp->nextSibling;

        if(temp == NULL)  //child not found
            FAIL() << "Child not found";
        else
            ASSERT_EQ(temp->number, child);
    }

    //destroy tree
    struct TreeNode *point1 = root->firstChild;
    struct TreeNode *point2 = root->nextSibling->nextSibling->firstChild;
    free(point2->nextSibling);
    free(point2);
    free(point1->nextSibling->nextSibling);
    free(point1->nextSibling);
    free(point1);
    free(root->nextSibling->nextSibling);
    free(root->nextSibling);
    free(root);
}
