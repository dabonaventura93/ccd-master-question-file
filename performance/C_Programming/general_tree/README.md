# C Programming: General Tree
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0073: Add and remove nodes from a Tree.
- S0052: Implement a function that returns a single value.
- S0072: Find an item in a Tree.
- S0059: Create and destroy a Tree.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

## Tasks
Implement a function `buildTree` that will create a general tree data structure using the `TreeNode` struct defined 
in `TestCode.h` and return the first tree node.

**PARAMETERS:**
1. `nums`: An array of integers used to construct TreeNode structures to be inserted into a tree
2. `size`: An int indicating the size of the array

**RETURN:** a `TreeNode` pointer to the first node in the tree

- The even indices, nums[0], nums[2], nums[4] etc will hold parent_numbers.
- The odd indices will hold numbers for each node.
  - So, nums[0] and nums[1] will be used to create a node, then nums[2] and nums [3] for the next node, etc.
- The tree should be built as you iterate through the array.
- The array will always have an even number of items.
- `size` will always match the size of the array.
- No duplicate values for `number` will be in the array.
- Nodes with a parent number > 0 will always have a matching parent node.

## General Tree
For this particular tree there will be only three levels:

1. Root - This is a pointer that points to the first Level 1 node.
2. Level 1 nodes - There can be zero to many Level 1 nodes directly under the root (considered children of the root). 
   Each of these Level 1 nodes can have zero to many children nodes at Level 2.
3. Level 2 nodes - There can be zero to many child nodes for each of the Level 1 nodes. These nodes will have NO 
   children.

Each node will contain an integer value stored in its "number" field. Nodes that are parents will point to the first 
child of the children nodes via the "firstChild" pointer (see illustration). All children nodes with the same parent 
are related through a sibling relationship which is defined as a "nextSibling" pointer in the TreeNode struct below 
(see illustration). Nodes are placed in the tree using the following rules:

1. If a node has a parent_number of 0, then it will be placed in Level 1 of the tree. 0 indicating that the root is 
   its parent.
2. If a node has a parent_number > 0, then it will be placed in Level 2 under the node in Level 1 containing that 
   value.

For example, if a node is added to the tree with parent_number = 0 and number = 7, it goes into Level 1. If the next 
node added has a parent_number of 7 and number of 33, then it goes into Level 2 of the tree under the node that has 
number=7. A populated tree may look something like this (the Node numbers are simply random labels):

```text
           ROOT
           Node 1                     Node 2                 Node 3      
           ---------               ----------               --------
Level 1    |p = 0  | ------------->|p = 0   |-------------->|p = 0 |
           |n = 44 |  nextSibling  |n = 22  |  nextSibling  |n = 7 |
           ---------               ---------                --------
             /                                                     \
            / firstChild                                            \ firstChild
           /                                                         \
          /                                                           \
         /  Node 4              Node 5                   Node 7        \ Node 6                 Node 8
        --------                ---------               ---------      ---------               --------
Level 2 |p = 44 | ------------->|p = 44 |-------------->|p = 44 |      | p = 7 |-------------->|p = 7 |
        |n = 10 |  nextSibling  |n = 17 |  nextSibling  |n = 2  |      | n = 33|  nextSibling  |n = 1 |
        ---------               ---------               ---------      ---------               --------
```

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
