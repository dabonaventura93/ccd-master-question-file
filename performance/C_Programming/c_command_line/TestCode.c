#include <stdio.h>
#include "testcases.h"
#include "TestCode.h"

// Refer to README.md for the problem instructions


int main(void)
{

    /* DO NOT MOVE OR MODIFY THE FOLLOWING CODE*/
    /* YOUR SOLUTION SHOULD BE CONTAINED ABOVE THIS POINT*/
    if (getenv("CCOMMANDLINETEST") == NULL)
    {
        doTest(argv[0]);
    }

    return 0;
}
