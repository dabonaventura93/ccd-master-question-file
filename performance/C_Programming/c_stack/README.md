# C Programming: C Stack
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0078: Push and pop a Stack.
- S0062: Create and destroy a Stack.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

## Tasks
This task will implement a simple stack using a linked list. A numNode struct is defined in the `TestCode.h` file to 
facilitate the creation of the stack.

### Task 1
Implement a `push` function that adds an item to the stack and returns a status code.

**PARAMETERS:**
1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack. 
2. `data`: an integer representing the new data to be added to the stack.

**RETURN:**  an integer containing `0` if successful, or `1` if `top` is null or memory is unavailable.

- update the `top` pointer so it points to the new top item in the stack.

### Task 2
Implement a `pop` function that removes the first item in the stack and returns the data stored in that item.

**PARAMETERS:**
1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack. 

**RETURN:** an integer containing `0` if `top` was null; otherwise, it is the data within the removed node

- update the `top` pointer so that it points to the next item in the stack. 
- If there are no remaining items, set `top` to NULL.

### Task 3
Implement an `emptyStack` function that removes all items in the stack.

**PARAMETERS:**
1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack.

**RETURN:** void

- Remove all nodes in the stack, freeing the memory used by the nodes. 
- Ensure the `top` pointer is updated to NULL after the function is done.
- Ensure the `top` parameter that is passed in is not null; if so, return.

### Task 4
Implement the function `createStack` that creates a stack according to the commands and data sent to it, and returns 
the top of the stack when it's finished.

**PARAMETERS:**
- `actions`: an int array - this array contains values in the even indices (0,2,4 etc.) that indicate which type of 
  action to do with the stack:

  1. pop the stack
  2. push onto the stack
  3. empty the stack

  - The odd indices will only apply for push actions. If the action received is action push (2), then the following 
    index contains the value to push onto the stack. The odd indices have no effect on pop (1) or empty stack (3)
                            
  - So if the function receives:  [2, 7, 2, 2, 1, 0, 3, 0], then you would push 7 onto the stack, then push 2 onto the 
    stack, then pop the stack, then empty the stack.

- `numActions`: an int representing the total number of actions supplied in the array

**RETURN:** a `struct numNode` pointer that represents the top of the stack or `null` if any action is not 1 - 3.

- The function should iterate the actions array and determine what actions should occur on the stack. 
- Use a 'switch' statement to determine which action to take on the stack and call the appropriate function 
  (i.e. `push`, `pop`, or `emptyStack`). 
- Assume that the array will always have an even number of elements. 

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.

