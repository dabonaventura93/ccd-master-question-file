#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    char *encodePigLatinPhrase(const char *englishPhrase);
#ifdef __cplusplus
}
#endif