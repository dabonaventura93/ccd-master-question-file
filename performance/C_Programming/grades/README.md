# C Programming: Grades
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

## Tasks
Implement a function `computeScore` that computes the average of a student’s score with different weights for each part 
and returns the course grade.

**PARAMETERS:**
1. `final`: An integer containing the score of the final
2. `midterm`: An integer containing the score of the midterm
3. `project`: An integer array of 6 elements containing the scores of the projects
4. `quiz`: An integer array of 4 elements containing the scores of the quizzes

**RETURN:** an integer containing the final grade rounded to the nearest integer or `ERROR_INVALID_DATA` (13) if any 
score is < 0 or > 100

- The function should compute the floating point before rounding the score to the nearest integer.

## Scoring
The overall score is computed as follows. 
- The student takes four (4) quizzes, six (6) projects, a Midterm exam, and a Final Exam. 
- The student’s lowest project score is discarded; therefore, the scoring only includes five (5) projects.
- Each score is in a range from 0 to 100. 
- The overall score is computed using the values below as such:
  - Final – 30%
  - Midterm – 25%
  - quizzes – 5% each
  - Projects – 5% each

overallScore = final * .30 + midterm * .25 + quiz1 * .05 + quiz2 * 05 + ....etc.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.

