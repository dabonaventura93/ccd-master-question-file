#define _CRT_SECURE_NO_WARNINGS 1
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include"TestCode.h"

// Refer to README.md for the problem instructions

void readData(HealthProfile *healthDatabase)
{
    
}

int calculateAgeInYears(HealthProfile hp)
{
    if (hp.dateOfBirth.day <= 0 || hp.dateOfBirth.month <= 0 || hp.dateOfBirth.year <= 0 ||
        hp.dateOfBirth.day > 31 || hp.dateOfBirth.month > 12)
        return 0;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    int currentMonth = tm.tm_mon + 1;
    int currentDay = tm.tm_mday;
    int currentYear = tm.tm_year + 1900;

    int age;

    // calculate the user's age
    if (hp.dateOfBirth.month <= currentMonth && hp.dateOfBirth.day <= currentDay)
        age = currentYear - hp.dateOfBirth.year;
    else
        age = currentYear - hp.dateOfBirth.year - 1;

    return age;
}

double calculateBMI(HealthProfile hp)
{

    return 0.0;
}

int calculateMaxHeartRate(int age)
{
   return 0;
}

double calculateMaxTargetHeartRate(int maxHR)
{
   return 0.0;
}

double calculateMinTargetHeartRate(int maxHR)
{
   return 0.0;
}

HealthProfile *processHealthProfiles()
{
    
    return NULL;
}
