#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    int validatePassword(const char *);

#ifdef __cplusplus
}
#endif