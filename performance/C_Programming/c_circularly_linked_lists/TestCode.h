#pragma once

#include <stdio.h>

struct numNode
{
    int  num;
    struct numNode *next;
};

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    struct numNode *buildCList(int *, int);
    int emptyList(struct numNode *);

#ifdef __cplusplus
}
#endif