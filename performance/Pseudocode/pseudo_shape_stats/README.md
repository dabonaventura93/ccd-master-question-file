# KSAT List
This question is intended to evaluate the following topics:
- A0004: Write pseudocode to solve a programming problem

# Tasks
## Scenario
You have been assigned to a new project that's still in the planning stage. The new project is a CAD type app that 
allows architects to create blue prints for various construction projects. Your current assignment requires a 
pseudocode writeup of the shape statistics requirements document to assist with the design of the statistics feature.

## Shape Statistics Requirements
This requirement provides architects with statistics of various shapes based on input shape data.
- Given input data; for example, shape and dimensions, the shape statistics feature will calculate the circumference 
  or perimeter, depending on the type of shape, and area of the shape.
- This feature will express calculated data in units of square feet.
- For simplicity, this feature will only support the following shapes, all 2D objects: square, rectangle, triangle, 
  and circle.
- The system will store the following dimensions for each shape:
  - The square and rectangle have length and width.
  - The triangle has base, side1 and side2.
  - The circle has radius.
- This feature will output the overall dimensions and calculated data in a way that's aesthetically pleasing and 
  easily understood.

## Hierarchy Chart
The chart below explains the code hierarchy for the Statistics Requirements feature.

```mermaid
graph TD;
    A["main()"].->C["other features . . ."];
    A--->B["get_shape_stats()"];
    B-->D["rectangle_area()"];
    B-->E["rectangle_perimeter()"];
    B-->F["triangle_area()"];
    B-->G["triangle_perimeter()"];
    B-->H["circle_area()"];
    B-->I["circle_circumference()"];
```
