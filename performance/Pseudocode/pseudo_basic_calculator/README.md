# KSAT List
This question is intended to evaluate the following topics:
- A0004: Write pseudocode to solve a programming problem
- A0018: Analyze a problem to formulate a software solution
- S0171: Write Pseudocode for a sequential process
- S0172: Write Pseudocode with repeating steps to the process

# Tasks
## Scenario
You have been assigned to a new project that's still in the planning stage. The new project is a basic calculator app. 
Your current assignment requires a pseudocode writeup of the basic calculator requirements document to assist with the 
design of the basic calculator feature.

## Basic Calculator Requirements
This requirement provides the necessary features for the basic calculator.
- The basic calculator shall compute add, subtract, multiply, and divide requests.
- The basic calculator shall be a command-prompt tool.
- Input shall be such that computations are entered as `NUM OP NUM<enter>`.
  - NUM is any number
  - OP is one of +, -, *, /
  - \<enter\> represents a user pressing the Enter key
- The result shall display on the command-prompt.
- The tool shall validate input is in the expected format (`NUM OP NUM`) and handle invalid cases.
  - The tool shall check for divide by zero cases
- The tool shall display error messages in a way the user can understand.

## Hierarchy Chart
The chart below explains the code hierarchy for the Basic Calculator feature.

```mermaid
flowchart TD;
    A["basicCalculator()"]<-->B["validateInput()"];
    A<--->C["add()"];
    A<--->D["subtract()"];
    A<--->E["multiply()"];
    A<--->F["divide()"];
```
