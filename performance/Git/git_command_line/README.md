# KSAT List
This question is intended to evaluate the following topics:
- S0018: Commit changes to a working branch.
- S0020: Update a project
- S0022: Clone a project
- S0386: Checkout an existing branch.

# Tasks
You have been tasked to finish the `basic_calc` core functions that will integrate with a `basic_calc.c` file being 
created by another one of your team members. Your team lead expects to see your changes in a branch called 
`finish_basic_calc`. You'll need to provide the [googletest](https://github.com/google/googletest.git) files 
necessary to allow the unit tests to build; note, the program expects to see these files in a `g_test` folder within 
this question's root directory. For the purposes of this question, issue a command to checkout the cloned `googletest` 
repository's `master` branch. 

During the last product owner review, a determination was made to modify the `long long divide(int, int)` function so 
it returns a double to make the results more precise. Your team lead will expect any applicable unit tests to be 
updated and pass. When you're finished with your modifications, use git commands necessary to update your repository 
(i.e. your exam repo) with the changes in your branch.

# Special Instructions
- For this question
  - You must use `git` on the command-line; you are not allowed to use the web interface to GitLab.com for git related 
    steps.
  - You are allowed, and required, to update `testcases.cpp` as indicated in the [Scenario](#scenario).
- In order to assist with grading, fill out the `git_answers.md` file with the git commands you 
used to solve this problem. 
- Save a copy of your bash history to a file called `git_cmd_line_history` using the following command 
`history > git_cmd_line_history`; ensure this file is included with the rest of your code.