#include "gtest/gtest.h"
#include "TestCode.h"

TEST(basic_calc_add, positiveCase)
{
    ASSERT_EQ(24, add(15, 9));
    ASSERT_EQ(0, add(0, 0));
    ASSERT_EQ(5, add(1, 4));
    ASSERT_EQ(4, add(0, 4));
    ASSERT_EQ(2353, add(2353, 0));
}

TEST(basic_calc_add, negativeCase)
{
    ASSERT_EQ(-12, add(-3, -9));
    ASSERT_EQ(-5, add(-6, 1));
    ASSERT_EQ(-2, add(4, -6));
    ASSERT_EQ(-5, add(0, -5));
    ASSERT_EQ(-421, add(-421, 0));
}

TEST(basic_calc_add, edgeCase)
{
    ASSERT_EQ(2147483648, add(1, 2147483647));
    ASSERT_EQ(-2147483648, add(-1, -2147483647));
}

TEST(basic_calc_sub, positiveCase)
{
    ASSERT_EQ(6, subtract(15, 9));
    ASSERT_EQ(-6, subtract(9, 15));
    ASSERT_EQ(8, subtract(9, 1));
    ASSERT_EQ(-8, subtract(1, 9));
    ASSERT_EQ(0, subtract(0, 0));
    ASSERT_EQ(-4, subtract(0, 4));
    ASSERT_EQ(2353, subtract(2353, 0));
}

TEST(basic_calc_sub, negativeCase)
{
    ASSERT_EQ(6, subtract(-3, -9));
    ASSERT_EQ(10, subtract(4, -6));
    ASSERT_EQ(7, subtract(1, -6));
    ASSERT_EQ(5, subtract(4, -1));
    ASSERT_EQ(5, subtract(0, -5));
    ASSERT_EQ(-421, subtract(-421, 0));
}

TEST(basic_calc_sub, edgeCase)
{
    ASSERT_EQ(-2147483648, subtract(-2147483647, 1));
    ASSERT_EQ(2147483648, subtract(2147483647, -1));
}

TEST(basic_calc_mul, positiveCase)
{
    ASSERT_EQ(135, multiply(15, 9));
    ASSERT_EQ(0, multiply(0, 0));
    ASSERT_EQ(0, multiply(0, 4));
    ASSERT_EQ(2353, multiply(2353, 1));
}

TEST(basic_calc_mul, negativeCase)
{
    ASSERT_EQ(27, multiply(-3, -9));
    ASSERT_EQ(-24, multiply(4, -6));
    ASSERT_EQ(0, multiply(0, -5));
    ASSERT_EQ(-421, multiply(-421, 1));
    ASSERT_EQ(421, multiply(-1, -421));
}

TEST(basic_calc_mul, edgeCase)
{
    ASSERT_EQ(2147483648, multiply(524288, 4096));
    ASSERT_EQ(-2147483648, multiply(-524288, 4096));
}

TEST(basic_calc_div, positiveCase)
{
    ASSERT_EQ(3, divide(15, 4));
    ASSERT_EQ(0, divide(0, 0));
    ASSERT_EQ(0, divide(1, 4));
    ASSERT_EQ(4, divide(4, 1));
    ASSERT_EQ(0, divide(0, 4));
    ASSERT_EQ(0, divide(2353, 0));
    ASSERT_EQ(8, divide(4096, 512));
}

TEST(basic_calc_div, negativeCase)
{
    ASSERT_EQ(0, divide(-3, -8));
    ASSERT_EQ(-6, divide(-6, 1));
    ASSERT_EQ(0, divide(3, -6));
    ASSERT_EQ(0, divide(0, -5));
    ASSERT_EQ(0, divide(-421, 0));
    ASSERT_EQ(-48, divide(1200, -25));
    ASSERT_EQ(-48, divide(-1200, 25));
    ASSERT_EQ(48, divide(-1200, -25));
}

TEST(basic_calc_mod, positiveCase)
{
    ASSERT_EQ(6, modulus(15, 9));
    ASSERT_EQ(0, modulus(0, 0));
    ASSERT_EQ(1, modulus(1, 4));
    ASSERT_EQ(0, modulus(4, 1));
    ASSERT_EQ(0, modulus(0, 4));
    ASSERT_EQ(2353, modulus(2353, 0));
    ASSERT_EQ(0, modulus(4096, 512));
}

TEST(basic_calc_mod, negativeCase)
{
    ASSERT_EQ(-3, modulus(-3, -9));
    ASSERT_EQ(0, modulus(-6, 1));
    ASSERT_EQ(4, modulus(4, -6));
    ASSERT_EQ(0, modulus(0, -5));
    ASSERT_EQ(-421, modulus(-421, 0));
    ASSERT_EQ(0, modulus(1200, -25));
    ASSERT_EQ(0, modulus(-1200, 25));
    ASSERT_EQ(0, modulus(-1200, -25));
}

TEST(basic_calc_isDigit, normalCase)
{
    const char *case1 = "234";
    const char *case2 = "0";
    ASSERT_EQ(1, isDigit(case1, strlen(case1)));
    ASSERT_EQ(1, isDigit(case2, strlen(case2)));
}

TEST(basic_calc_isDigit, invalidCase)
{
    const char *case1 = "23.4";
    const char *case2 = ".32";
    const char *case3 = "just a string";
    const char *case4 = "just a 6 string";
    ASSERT_EQ(0, isDigit(case1, strlen(case1)));
    ASSERT_EQ(0, isDigit(case2, strlen(case2)));
    ASSERT_EQ(0, isDigit(case3, strlen(case3)));
    ASSERT_EQ(0, isDigit(case4, strlen(case4)));
}

TEST(basic_calc_isDigit, nullCase)
{
    ASSERT_EQ(0, isDigit(NULL, 0));
    ASSERT_EQ(0, isDigit("", 0));
}
