# KSAT List
This question is intended to evaluate the following topics:
- K0627: User Story Development
- S0184: Develop a user story from a requirement.
- S0384: Create an issue.
- S0385: Create a merge request.

# Tasks
You've previously developed a website that uses 3 buttons to start, pause, and stop an animation. You just got your 
first color blind user and they can't tell which buttons do what actions as they are all sized and shaped exactly the 
same.  You use a separate key and legend to say which colors mean what with only color as the difference.

Create an issue in your exam repository on gitlab.com to answer this question. In the issue description, create a User 
Story that describes the requirement and a potential high level solution. You do not need to implement this solution. 

- The issue must contain an acceptance criteria.
- Assign yourself to the issue.
- The issue title must not contain the terms "master", "exam", "test", "branch", or your name

After you create the issue, create a merge request from a new branch based off of master. **THIS BRANCH MUST NOT BE 
YOUR EXAM BRANCH**. The branch name must NOT contain the terms "master", "exam", "test", "branch", or your name.

- Ensure the merge request is marked as Ready
- Assign the merge request to an exam administrator
